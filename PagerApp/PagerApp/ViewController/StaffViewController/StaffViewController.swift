//
//  StaffViewController.swift
//  PagerApp
//
//  Created by Mac on 26/11/2021.
//

import UIKit
import AVFoundation
import AVKit

struct Objects {
    
    let sectionName : String
    var sectionObjects : [showStaffDetail] = []
    
}


class StaffViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var tblStaff: UITableView!
    @IBOutlet weak var collectionViewVideo: UICollectionView!
    @IBOutlet weak var heightTblStaff: NSLayoutConstraint!
    @IBOutlet weak var constrainsBtnAddStaff: NSLayoutConstraint!
    @IBOutlet weak var heightThemeContainerview: NSLayoutConstraint!
    @IBOutlet weak var btnAddStaff: UIButton!
    @IBOutlet weak var viewReport: UIView!
    @IBOutlet weak var themeContainerView: UIView!
    @IBOutlet weak var OptionsView: UIView!
    @IBOutlet weak var lblBusinessTitle: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var myUser:[User]?{didSet{}}
    var arrShowStaff:[showStaffDetail] = []
    var objectArray = [Objects]()
    let spinner = UIActivityIndicatorView(style: .gray)
    var pageNumber = 0
    var totalPages = 1
    var isDataLoading:Bool=false
    
    
    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.myUser = User.readUserFromArchive()
        self.btnAddStaff.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.viewReport.backgroundColor = UIColor.ultralightGreen
        self.viewReport.layer.cornerRadius = 2
        self.themeContainerView.isHidden =  true
        self.OptionsView.isHidden = true
        self.constrainsBtnAddStaff.constant = 20
        self.tblStaff.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        self.lblBusinessTitle.setTitle(self.myUser?[0].business_name, for: .normal)
        
        //refresher
        spinner.color = UIColor.white
        spinner.hidesWhenStopped = true
        tblStaff.tableFooterView = spinner
        self.pageNumber = 0
        self.showStaffs(starting_point: "0")
    }
    

    //MARK:- Button Action
    @IBAction func btnAddStaffAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier:"AddStaffViewController") as! AddStaffViewController
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnReportAction(_ sender: Any) {
        self.actionReportPayement()
    }
    @IBAction func btnBulkPayement(_ sender: Any) {
        self.actionSheetBulkPayement()
    }
    @IBAction func btnPayementLog(_ sender: Any) {
        
    }
    @IBAction func btnSwitchBusinessAction(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "SwitchAddBusinessViewController") as! SwitchAddBusinessViewController
        vc.arrBusiness =  (self.myUser?[0].Business!)!
        vc.hidesBottomBarWhenPushed =  true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- DELEGATE METHODS
    
    //MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objectArray.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objectArray[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staffTableViewCell") as! staffTableViewCell
        cell.setupData(arrData: self.objectArray[indexPath.section].sectionObjects[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "StaffProfileDetailViewController") as! StaffProfileDetailViewController
        vc.hidesBottomBarWhenPushed =  true
        vc.navigationTitle = self.objectArray[indexPath.section].sectionObjects[indexPath.row].staffname!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as? HeaderView
        header?.lblSection.text = "\(self.objectArray[section].sectionName.capitalizingFirstLetter())(\(self.objectArray[section].sectionObjects.count))"
        return header
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:collectionViewVideo.layer.bounds.width/1.2, height: collectionViewVideo.layer.bounds.width)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var url:NSURL = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")!
        
        let player = AVPlayer(url: url as URL)
        let vc = AVPlayerViewController()
        vc.player = player
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true) { vc.player?.play() }
    }
    
    //MARK: Alert View
    func actionSheetBulkPayement(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let PayOnline =  UIAlertAction(title: "Pay Online", style: .default) { UIAlertAction in
            
        }
        let saveOfflineEntry =  UIAlertAction(title: "Save Offline Entry", style: .default) { UIAlertAction in
            
        }
        
        let Cancel =  UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        Cancel.setValue(UIColor.red, forKey: "titleTextColor")
        
        alert.addAction(PayOnline)
        alert.addAction(saveOfflineEntry)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func actionReportPayement(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let PayOnline =  UIAlertAction(title: "Attendence Report", style: .default) { UIAlertAction in
            
        }
        let saveOfflineEntry =  UIAlertAction(title: "Payment Report", style: .default) { UIAlertAction in
            
        }
        
        let Cancel =  UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        Cancel.setValue(UIColor.red, forKey: "titleTextColor")
        
        alert.addAction(PayOnline)
        alert.addAction(saveOfflineEntry)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: API Handler
    
    func showStaffs(starting_point:String){
        self.myUser = User.readUserFromArchive()
        
        let currentDate = NSDate()
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd"
        let strdate =  formater.string(from: currentDate as Date)
        
        
        if AppUtility!.connected() == false{
            return
        }
        let strId =   self.myUser?[0].business_id!
        ApiHandler.sharedInstance.showStaffs(business_id: strId!,starting_point: starting_point,strDate:strdate) { (isSuccess, resp) in
            print(resp)
            
            if isSuccess {
                if starting_point == "0"{
                    self.arrShowStaff.removeAll()
                }
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                        
                        if let monthdata = res["monthly"]  as? [[String:Any]]{
                            for obj in monthdata {
                                let obj = showStaffDetails.shared.ObjUserDetailResponse(res: obj)
                                self.arrShowStaff.append(obj)
                            }
                        }
                        if let weeklydata = res["weekly"]  as? [[String:Any]]{
                            for obj in weeklydata {
                                let obj = showStaffDetails.shared.ObjUserDetailResponse(res: obj)
                                self.arrShowStaff.append(obj)
                            }
                        }
                        if let hourlydata = res["hourly"]  as? [[String:Any]]{
                            for obj in hourlydata {
                                let obj = showStaffDetails.shared.ObjUserDetailResponse(res: obj)
                                self.arrShowStaff.append(obj)
                            }
                        }

                        var section = self.arrShowStaff.map{$0.cycle_type as! String}
                        section.removeDuplicates()
                        self.objectArray = section.map{ category in
                            print("Cat: ", category)
                            let obj = self.arrShowStaff.filter{ ($0.cycle_type) == category}
                            return Objects(sectionName: category, sectionObjects: obj)
                            
                        }
                        
                        let obj = CGFloat(self.arrShowStaff.count * 76)
                        let Section  =  CGFloat(self.objectArray.count * 40) + 40
                        self.heightTblStaff.constant =   obj + Section
                        
                        self.constrainsBtnAddStaff.constant = 20
                        self.themeContainerView.isHidden =  true
                        self.OptionsView.isHidden = false
                        
                        self.tblStaff.reloadData()
                        
                    }else{
                        AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg") as? String ?? "", delegate: self)
                    }
                }else{
                    self.themeContainerView.isHidden =  false
                    self.OptionsView.isHidden = true
                    self.constrainsBtnAddStaff.constant = 150
                }
            }
        }
    }
}


extension StaffViewController {
    //MARK: ScrollView Delegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        print("scrollViewWillBeginDragging")
        
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        print("scrollViewDidEndDragging")
        if scrollView == self.tblStaff{
            if ((tblStaff.contentOffset.y + tblStaff.frame.size.height) >= tblStaff.contentSize.height)
            {
                if !isDataLoading{
                    isDataLoading = true
                    spinner.startAnimating()
                    print("Next page call")
                    if self.pageNumber < self.totalPages{
                        self.pageNumber = self.pageNumber + 1
                        
                        self.showStaffs(starting_point: "\(self.pageNumber)")
                    }else{
                        self.spinner.hidesWhenStopped = true
                        self.spinner.stopAnimating()
                    }
                }
            }
        }
    }
}

//MARK:- remove duplicate elements in Array
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
