//
//  staffHeaderViewController.swift
//  PagerApp
//
//  Created by Mac on 26/11/2021.
//

import UIKit

class staffHeaderViewController: UIViewController {
    @IBOutlet weak var lblDescription: UILabel!
    let textDescription = ["Add Staff(Monthly, Weekly ,Hourly ,Daily and per Piece Workers)","Maintain Salary,Advance and Pending payments","Mark Attendeance,Overtime and Late Defuctions","Send Payments & Attendance Reports via Whatsapp& SMS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDescription.attributedText = AppUtility?.add(stringList: textDescription, font: lblDescription.font, bullet: "🔵")
    }
    //MARK:- NSAttributed Strings


}
