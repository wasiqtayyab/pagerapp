//
//  staffTableViewCell.swift
//  PagerApp
//
//  Created by Mac on 26/11/2021.
//

import UIKit

class staffTableViewCell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblStaffStatus: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAmountStatus: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func setupData(arrData:showStaffDetail){
        
        self.lblname.text =  arrData.staffname
        self.lblAmount.text = "$ \(arrData.salary!)"
        
        if arrData.attendenceId == 0 {
            self.lblStaffStatus.text = "Not Marked"
        }else{
            self.lblStaffStatus.text = "Present"
        }
        if arrData.opening_balance_type == "0" {
            self.lblAmountStatus.text = "Advance"
        }else{
            self.lblAmountStatus.text = "Pending"
        }
    }
}
