//
//  VideoCollectionViewCell.swift
//  PagerApp
//
//  Created by Mac on 26/11/2021.
//

import UIKit

class VideoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var imgPlayButton: UIImageView!
    
}
