//
//  AddBusinessViewController.swift
//  PagerApp
//
//  Created by Mac on 17/11/2021.
//

import UIKit

class AddBusinessViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfBusinessName: UITextField!
    @IBOutlet weak var tfStaffNumber: UITextField!
    @IBOutlet weak var btnContinue: RNLoadingButton!
    @IBOutlet weak var btnTicked: UIButton!
    @IBOutlet weak var imgTicked: UIImageView!
    
    
    //MARK:- VARS
    
    var isTicked = true
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
     
    }
    
    //MARK:- SETUP VIEW
    
    func setupView(){
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        imgTicked.isHidden = false
        
    }
    
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
       
        if AppUtility!.isEmpty(self.tfName.text!){
            AppUtility?.showToast(string: NSLocalizedString("validation_empty_UserName", comment: ""), view: self.view)
            self.tfName.shake()
            return
        }
        
        if AppUtility!.isEmpty(self.tfBusinessName.text!){
            AppUtility?.showToast(string: NSLocalizedString("validation_empty_business_name", comment: ""), view: self.view)
            self.tfBusinessName.shake()
            return
        }
        
        if AppUtility!.isEmpty(self.tfStaffNumber.text!){
            AppUtility?.showToast(string: NSLocalizedString("validation_empty_staffNumber", comment: ""), view: self.view)
            self.tfStaffNumber.shake()
            return
        }
        
        var obj = [String:Any]()
        obj = [
            "name": self.tfName.text!,
            "businessname" : self.tfBusinessName.text!,
            "staffnumber": self.tfStaffNumber.text!
        ]
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "StaffHoursViewController") as! StaffHoursViewController
        vc.objBusiness = obj
        self.navigationController?.pushViewController(vc, animated: true)
        
       
    }
    
    @IBAction func termConditionButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func tickedButtonPressed(_ sender: UIButton) {
        if isTicked == true {
            
            btnTicked.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1)
            btnTicked.layer.borderWidth = 1.5
            btnTicked.layer.cornerRadius = 8
            imgTicked.isHidden = true
            isTicked = false
        }else {
            
            imgTicked.isHidden = false
            isTicked = true
        }
    }
}
