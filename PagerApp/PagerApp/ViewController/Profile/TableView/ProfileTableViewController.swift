//
//  ProfileTableViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 01/12/2021.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    @IBOutlet var tblProfile: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProfile.tableFooterView = UIView()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 : 20.0
    }
    
   
    
}

