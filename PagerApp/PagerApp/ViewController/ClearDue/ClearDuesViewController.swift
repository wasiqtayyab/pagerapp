//
//  ClearDuesViewController.swift
//  PagerApp
//
//  Created by Mac on 01/12/2021.
//

import UIKit
import DatePickerDialog

class ClearDuesViewController: UIViewController {
    
    
    //MARK: Outlets
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var btnSave: RNLoadingButton!
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    @IBOutlet weak var btnShowDate: UIButton!
    let datePicker = DatePickerDialog()
    
    var isTick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnSave.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.viewOnline.layer.borderColor = UIColor.primary.cgColor
        self.viewOnline.layer.borderWidth = 1
        self.viewOnline.layer.cornerRadius = 8
    }
    
    //MARK: Switch Action
    
    //MARK: Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDatePicker(_ sender: Any) {
        self.datePickerTapped()
    }
    @IBAction func btnTermsCondrion(_ sender: Any) {
        
    }
    @IBAction func btnSendSMSAction(_ sender: Any) {
        if isTick == true{
            self.isTick = false
            self.imgTick.image = UIImage(named: "filled_circle")
        }else{
            self.isTick = true
            self.imgTick.image = UIImage(named: "square_tick")
        }
    }
    
    //MARK: DELEGATE METHODS
    func datePickerTapped() {
     
        let currentDate = Date()
        
        var dateComponents = DateComponents()
        dateComponents.month = -3
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        datePicker.show("DatePickerDialog",doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo,maximumDate:currentDate,datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MMM-yyyy"
                let selectDate = formatter.string(from: dt)
                self.btnShowDate.setTitle(selectDate, for: .normal)
            }
        }
    }
    //MARK: TableView
    
    //MARK: CollectionView.
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    
}
