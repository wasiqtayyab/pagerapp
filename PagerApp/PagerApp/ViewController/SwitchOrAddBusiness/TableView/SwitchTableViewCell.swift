//
//  SwitchTableViewCell.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 18/11/2021.
//

import UIKit

class SwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblStaff: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
