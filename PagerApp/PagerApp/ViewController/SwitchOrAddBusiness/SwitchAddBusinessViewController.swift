//
//  SwitchAddBusinessViewController.swift
//  PagerApp
//
//  Created by Mac on 18/11/2021.
//

import UIKit

class SwitchAddBusinessViewController: UIViewController{
    
    //MARK:- OUTLET

    @IBOutlet weak var tblSwitchBusiness: UITableView!
    @IBOutlet weak var btnAddBusiness: UIButton!
    var mySwitchBusiness:[switchBusiness]?{didSet{}}
    var myUser:[User]?{didSet{}}
    var arrBusiness = [[String:Any]]()
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mySwitchBusiness =  switchBusiness.readswitchBusinessFromArchive()
        tblSwitchBusiness.delegate = self
        tblSwitchBusiness.dataSource = self
        tblSwitchBusiness.tableFooterView = UIView()
        self.btnAddBusiness.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNewBusinessAction(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AddNewBusinessViewController") as! AddNewBusinessViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- TABLE VIEW DELEGATE
extension SwitchAddBusinessViewController:UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBusiness.count //self.mySwitchBusiness!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell", for: indexPath)as! SwitchTableViewCell
        cell.lblStaff.text = "\(self.arrBusiness[indexPath.row]["total_staff"] as! String) staffs"  //"\(self.mySwitchBusiness![indexPath.row].total_staff as! String) staffs"
        cell.lblBusinessName.text =   self.arrBusiness[indexPath.row]["name"] as! String //self.mySwitchBusiness![indexPath.row].name as! String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      /*  let obj =  self.mySwitchBusiness![indexPath.row]
        self.myUser = User.readUserFromArchive()
        self.myUser?[0].business_id = obj.id
        self.myUser?[0].business_type_id =  obj.business_type_id
        self.myUser?[0].user_id =  obj.user_id
        self.myUser?[0].business_name = obj.name
        self.myUser?[0].total_staff = obj.total_staff
        self.myUser?[0].month_calculation = obj.month_calculation
        self.myUser?[0].working_hours =  obj.working_hours
        self.myUser?[0].business_public_holiday_template_id =  obj.business_public_holiday_template_id
        self.myUser?[0].updated =  obj.updated
        self.myUser?[0].business_created =  obj.created
        self.myUser?[0].business_staff_count = obj.business_staff_count
        
        if User.saveUserToArchive(user: self.myUser!){
            print("switch account and user obj update with:\(self.myUser?[0].business_name)")
            self.navigationController?.popViewController(animated: true)
        }*/
        self.myUser = User.readUserFromArchive()
        let obj =  self.arrBusiness[indexPath.row]
        self.myUser?[0].business_id = obj["id"] as? String ?? "0"
        self.myUser?[0].business_type_id =  obj["business_type_id"] as? String ?? "0"
        self.myUser?[0].user_id =  obj["user_id"] as? String ?? "0"
        self.myUser?[0].business_name =  obj["name"] as? String ?? "0"
        self.myUser?[0].total_staff =  obj["total_staff"] as? String ?? "0"
        self.myUser?[0].month_calculation =  obj["month_calculation"] as? String ?? "0"
        self.myUser?[0].working_hours =  obj["working_hours"] as? String ?? "0"
        self.myUser?[0].business_public_holiday_template_id = obj["business_public_holiday_template_id"] as? String ?? "0"
        self.myUser?[0].updated =  obj["updated"] as? String ?? "0"
        self.myUser?[0].business_created = obj["created"] as? String ?? "0"
        self.myUser?[0].business_staff_count =  obj["business_staff_count"] as? Int ?? 0
          
          if User.saveUserToArchive(user: self.myUser!){
              print("switch account and user obj update with:\(self.myUser?[0].business_name)")
              self.navigationController?.popViewController(animated: true)
          }
    }
}
