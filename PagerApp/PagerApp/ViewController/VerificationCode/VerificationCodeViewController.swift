//
//  VerificationCodeViewController.swift
//  PagerApp
//
//  Created by Mac on 17/11/2021.
//

import UIKit
import SVPinView
class VerificationCodeViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var otpView: SVPinView!
    @IBOutlet weak var btnVerifyNumber: RNLoadingButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    var myUser:[User]?{didSet{}}
    var strPhoneNumber = ""
    var OTPView = ""
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    //MARK:- FUNCTION
    
    
    func setupView(){
        self.myUser = User.readUserFromArchive()
        self.lblNumber.text! = self.strPhoneNumber
        otpView.didFinishCallback = { [weak self] pin in
            self?.OTPView = pin
            self!.btnVerifyNumber.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
            self!.btnVerifyNumber.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }
        
        otpView.didChangeCallback = { [weak self] pin in
            self?.OTPView = ""
            self!.btnVerifyNumber.backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8392156863, alpha: 1)
            self!.btnVerifyNumber.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        }
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func verifyNumberButtonPressed(_ sender: UIButton) {
        if OTPView == ""{
            AppUtility?.showToast(string: NSLocalizedString("validation_empty_OTP", comment: ""), view: self.view)
            self.otpView.shake()
            return
        }
        self.phoneNumberVerifyApi()
        
    }
    //MARK:- API Handler
    
func phoneNumberVerifyApi(){
    
    if AppUtility!.connected() == false{
        return
    }
    self.btnVerifyNumber.isLoading =  true
    
    ApiHandler.sharedInstance.verifyPhoneNumber(verify:"1",phone:self.strPhoneNumber,strCode:self.OTPView) { (isSuccess, resp) in
      
        if isSuccess {
            self.btnVerifyNumber.isLoading =  false
            
            if resp?.value(forKey: "code") as! NSNumber == 200{
                if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                    UserObject.shared.Objresponse(response: res)
                    DispatchQueue.main.async {
                        self.myUser =  User.readUserFromArchive()
                        if self.myUser != nil && self.myUser?.count != 0{
                            if  let role  = self.myUser?[0].Role{
                                  if role["name"] as! String ==  "admin"{
                                      if self.myUser![0].Business?.count == 0{
                                          let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AddBusinessViewController") as! AddBusinessViewController
                                          self.navigationController?.pushViewController(vc, animated: true)
                                      }else{
                                          let vc =  self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                                          let nav = UINavigationController(rootViewController: vc)
                                          nav.navigationBar.isHidden = true
                                          self.view.window?.rootViewController = nav
                                      }
                                  }
                              }
                            
                        }
                    }
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
            }else{
                print(resp as Any)
            }
        }
    }
  }
}
