//
//  StaffNameViewController.swift
//  PagerApp
//
//  Created by Mac on 02/12/2021.
//

import UIKit

class StaffEditNameViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tfStaffName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK: Switch Action
    
    //MARK: Button Action
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: DELEGATE METHODS
    
    //MARK: TableView
    
    //MARK: CollectionView.
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    
}
