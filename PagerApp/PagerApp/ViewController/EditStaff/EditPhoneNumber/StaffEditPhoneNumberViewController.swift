//
//  StaffPhoneNumberViewController.swift
//  PagerApp
//
//  Created by Mac on 02/12/2021.
//

import UIKit
import ContactsUI
import Contacts

class StaffEditPhoneNumberViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tfPhonenumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnAddContact.layer.borderColor = UIColor.primary.cgColor
        self.btnAddContact.layer.borderWidth = 1
        self.btnAddContact.layer.cornerRadius = 8
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK: Switch Action
    
    //MARK: Button Action
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnAddContact(_ sender: Any) {
        self.onClickPickContact()
    }
    
}
extension StaffEditPhoneNumberViewController:CNContactPickerDelegate{
    
    //MARK:- contact picker
    func onClickPickContact(){
        
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys = [CNContactGivenNameKey , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
     func contactPicker(_ picker: CNContactPickerViewController,
                              didSelect contactProperty: CNContactProperty) {
        
    }
    
     func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        // user name
        let userName:String = contact.givenName
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        
        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
        self.tfPhonenumber.text = primaryPhoneNumberStr
      
        print(primaryPhoneNumberStr)
      
        
    }
    
     func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
}

