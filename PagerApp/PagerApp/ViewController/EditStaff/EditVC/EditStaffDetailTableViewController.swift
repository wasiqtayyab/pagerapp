//
//  EditStaffDetailTableViewController.swift
//  PagerApp
//
//  Created by Mac on 02/12/2021.
//

import UIKit

class EditStaffDetailTableViewController: UITableViewController {

    @IBOutlet var tbl: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbl.tableFooterView = UIView()
    }

    // MARK: - Button action

    @IBAction func btnEditNameAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffEditNameViewController") as!  StaffEditNameViewController
        self.navigationController?.present(vc, animated: true)
    }
    @IBAction func btnEditPhonenumberAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffEditPhoneNumberViewController") as!
        StaffEditPhoneNumberViewController
        self.navigationController?.present(vc, animated: true)
    }
    @IBAction func btnOtherDetails(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffOtherDetailsViewController") as!
        StaffOtherDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
