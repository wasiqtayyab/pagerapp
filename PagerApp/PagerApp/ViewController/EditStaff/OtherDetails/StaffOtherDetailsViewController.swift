//
//  StaffOtherDetailsViewController.swift
//  PagerApp
//
//  Created by Mac on 02/12/2021.
//

import UIKit

class StaffOtherDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    //MARK: Outlets
    
    @IBOutlet weak var tbl: UITableView!
    
    
    
    //MARK: ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl.register(OtherDetailsTableHeaderView.self, forHeaderFooterViewReuseIdentifier: "OtherDetailsTableHeaderView")
    }
    
    //MARK: Button Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: TableView
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2{
            return 3
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffOtherDetailsTableViewCell") as! StaffOtherDetailsTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header  =  tableView.dequeueReusableHeaderFooterView(withIdentifier: "OtherDetailsTableHeaderView") as! OtherDetailsTableHeaderView
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 75
    }
    
}
