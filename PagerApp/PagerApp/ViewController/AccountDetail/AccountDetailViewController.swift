//
//  AccountDetailViewController.swift
//  PagerApp
//
//  Created by Mac on 01/12/2021.
//

import UIKit

class AccountDetailViewController: UIViewController {

   
   
    //MARK: Outlets
    @IBOutlet weak var segmentBank: UISegmentedControl!
    @IBOutlet weak var viewBankAccount: UIView!
    @IBOutlet weak var viewUPI: UIView!
    @IBOutlet weak var btnVerify: RNLoadingButton!
    
    
    @IBOutlet weak var tfAccountHolderName: UITextField!
    @IBOutlet weak var tfAccountNumber: UITextField!
    @IBOutlet weak var tfConfirmAccountNumber: UITextField!
    @IBOutlet weak var tfIFSCCode: UITextField!
    
    @IBOutlet weak var tfUPIID: UITextField!
    
    //MARK: ViewDidLoad
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
        
 
        
          self.setupView()
      }
      
      //MARK: SetupView
      
      func setupView(){
          self.btnVerify.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
          self.segmentBank.selectedSegmentIndex = 0
          self.viewUPI.isHidden =  true
          self.viewBankAccount.isHidden =  false
      }
      
      //MARK: Switch Action

      //MARK: Button Action
    @IBAction func segmentValueChanged(_ sender: Any) {
        if segmentBank.selectedSegmentIndex == 0 {
            self.viewUPI.isHidden =  true
            self.viewBankAccount.isHidden =  false
        
        }else{
            self.viewUPI.isHidden =  false
            self.viewBankAccount.isHidden =  true
        
        }
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnVerify(_ sender: Any) {
        self.btnVerify.isLoading =  true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.btnVerify.isLoading =  false
            self.navigationController?.popViewController(animated: true)
            
        }
        
       
    }
      //MARK: DELEGATE METHODS
      
      //MARK: TableView
      
      //MARK: CollectionView.
      
      //MARK: Segment Control
      
      //MARK: Alert View
      
      //MARK: TextField
      
      //MARK: Location
        
      //MARK: Google Maps
    
     
}
