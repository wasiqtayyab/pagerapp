//
//  StaffDetailOptionCollectionViewCell.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import UIKit

class StaffDetailOptionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbltext: UILabel!
}
