//
//  StaffDetailViewController.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import UIKit
import AVFoundation
import AVKit

class StaffProfileDetailViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
 
    

    
    
    //MARK: Outlets
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    @IBOutlet weak var collectionViewVideo: UICollectionView!
    @IBOutlet weak var lblnavigationTitle: UILabel!
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet weak var btnAddAccountDetail: UIButton!
    var arr = [["logo":"OVerTime","text":"Overtime"],["logo":"Allowance","text":"Allowance/Bonus"],["logo":"Deduction","text":"Deduction"],["logo":"Loan","text":"Loan Entry"]]
    var navigationTitle = ""
    
   
    
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.tbl.register(UINib(nibName: "staffClearDuesTableFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "staffClearDuesTableFooterView")
        self.btnAddAccountDetail.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.lblnavigationTitle.text =  self.navigationTitle
        self.btnAddContact.layer.borderColor = UIColor.primary.cgColor
        self.btnAddContact.layer.borderWidth = 1
        self.btnAddContact.layer.cornerRadius = 8
        self.heightTableView.constant =  (2*76) + 76
    }
    
    
    //MARK: Button Actions
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddPreviousMonthAction(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "AddPreviousMonthViewController") as! AddPreviousMonthViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnAddAccountDetail(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "AccountDetailViewController") as! AccountDetailViewController
        self.navigationController?.pushViewController(vc , animated: true)
    }
    @IBAction func btnEditProfile(_ sender: Any) {
        let vc =  storyboard?.instantiateViewController(withIdentifier: "EditStaffDetailViewController") as! EditStaffDetailViewController
        self.navigationController?.pushViewController(vc , animated: true)
    }
    
    
    //MARK: Delegate
    
    @objc func btnClearDues(btn:UIButton){
        let vc =  storyboard?.instantiateViewController(withIdentifier: "ClearDuesViewController") as! ClearDuesViewController
        self.navigationController?.pushViewController(vc , animated: true)
    }
    
    //MARK: TableView
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staffTableViewCell") as! staffTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "staffClearDuesTableFooterView") as! staffClearDuesTableFooterView
        footer.btnClearDues.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        footer.btnClearDues.tag =  section
        footer.btnClearDues.addTarget(self, action: #selector(self.btnClearDues(btn:)), for: .touchUpInside)
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 75
    }
    
    
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewVideo {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StaffDetailOptionCollectionViewCell", for: indexPath) as! StaffDetailOptionCollectionViewCell
            cell.img.image = UIImage(named:self.arr[indexPath.row]["logo"] as! String )
            cell.lbltext.text =  self.arr[indexPath.row]["text"] as! String
            return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewVideo {
            return CGSize(width:collectionViewVideo.layer.bounds.width/1.2, height: collectionViewVideo.layer.bounds.width)
        }else{
            let size  =  self.collectionView.layer.bounds.width/4
            return CGSize(width:size, height: size)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewVideo {
            var url:NSURL = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")!
            
            let player = AVPlayer(url: url as URL)
            let vc = AVPlayerViewController()
            vc.player = player
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true) { vc.player?.play() }
        }
        
        
    }
    
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    
}
