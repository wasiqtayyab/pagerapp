//
//  MonthlySalaryCalculationViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 18/11/2021.
//

import UIKit

class MonthlySalaryCalculationViewController: UIViewController {

    //MARK:- OUTLET
    
    @IBOutlet weak var calendarMonthView: UIView!
    @IBOutlet weak var lblCalendarMonth: UILabel!
    @IBOutlet weak var lblDateMonth: UILabel!
    @IBOutlet weak var btnCircle: UIButton!
    @IBOutlet weak var imgCircleTicked: UIImageView!
    @IBOutlet weak var btnCalendarMonth: UIButton!
    
    
    @IBOutlet weak var monthDaysView: UIView!
    @IBOutlet weak var lbl30Days: UILabel!
    @IBOutlet weak var lblDate30: UILabel!
    @IBOutlet weak var btnCircleMonth: UIButton!
    @IBOutlet weak var imgCircleMonthTicked: UIImageView!
    @IBOutlet weak var btnMonth30Day: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK:- SETUP FUNCTION
    
    func setupView(){
        
        calendarMonthView.layer.borderWidth = 1.5
        calendarMonthView.layer.borderColor = #colorLiteral(red: 0.03529411765, green: 0.5176470588, blue: 1, alpha: 1)
        lblCalendarMonth.textColor = UIColor(named: "blue")
        calendarMonthView.layer.cornerRadius = 5
        
        monthDaysView.layer.borderWidth = 1.5
        monthDaysView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        monthDaysView.layer.cornerRadius = 5
        
        
    }

    
    //MARK:- BUTTON ACTION
    
    @IBAction func createButtonPressed(_ sender: UIButton) {
    }
   
    @IBAction func calendarMonthButtonPressed(_ sender: UIButton) {
        
        lblCalendarMonth.textColor = UIColor(named: "blue")
        calendarMonthView.layer.borderColor = #colorLiteral(red: 0.03529411765, green: 0.5176470588, blue: 1, alpha: 1)
        imgCircleTicked.isHidden = false
        imgCircleTicked.image = UIImage(named: "55 (9)")
        
        lbl30Days.textColor = .black
        monthDaysView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        imgCircleMonthTicked.isHidden = true
        
    }
    
    @IBAction func month30DayButtonPressed(_ sender: UIButton) {
        
        lbl30Days.textColor = UIColor(named: "blue")
        monthDaysView.layer.borderColor = #colorLiteral(red: 0.03529411765, green: 0.5176470588, blue: 1, alpha: 1)
        imgCircleMonthTicked.isHidden = false
        imgCircleMonthTicked.image = UIImage(named: "55 (9)")
        
        lblCalendarMonth.textColor = .black
        calendarMonthView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        imgCircleTicked.isHidden = true
       
    }

}
