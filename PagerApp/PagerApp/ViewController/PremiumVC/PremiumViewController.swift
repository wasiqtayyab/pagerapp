//
//  PremiumViewController.swift
//  PagerApp
//
//  Created by Mac on 20/12/2021.
//

import UIKit
import AVFoundation
import AVKit

class PremiumViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var lblStrikePrice: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAnnualSubscription: UILabel!
    @IBOutlet var viewCard: [UIView]!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var btnPayNow1: RNLoadingButton!
    
    
    let textDescription = ["Limited time offer!","Your Trial Period gets added to the Annual Subscription"]
    
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewCard.forEach { (card) in
            card.layer.borderColor = UIColor(named: "lightGrayColor")?.cgColor
            card.layer.borderWidth = 1.0
            
        }
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnPayNow1.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.lblAnnualSubscription.attributedText = AppUtility?.add(stringList: textDescription, font: lblAnnualSubscription.font, bullet: "•")
        self.btnPayNow.layer.borderWidth = 1.0
        self.btnPayNow.layer.borderColor = UIColor.primary.cgColor
    }
    
 
    //MARK: Button Action
    @IBAction func btnPayNowAction(_ sender: Any) {
    
    }
    
 
    //MARK: TableView
    
   
    //MARK: CollectionView.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionViewCell", for: indexPath) as! VideoCollectionViewCell
            return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            return CGSize(width:videoCollectionView.layer.bounds.width/1.2, height: videoCollectionView.layer.bounds.width)
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
            var url:NSURL = NSURL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v")!
            
            let player = AVPlayer(url: url as URL)
            let vc = AVPlayerViewController()
            vc.player = player
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true) { vc.player?.play() }
      
        
    }
}
