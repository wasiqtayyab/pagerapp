//
//  LoginAccountViewController.swift
//  PagerApp
//
//  Created by Mac on 17/11/2021.
//

import UIKit
import PhoneNumberKit
import EFInternetIndicator

class LoginAccountViewController: UIViewController,InternetStatusIndicable {
   
    //MARK:- OUTLET
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var btnContinue: RNLoadingButton!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    var internetConnectionIndicator: InternetViewIndicator?
    let phoneNumberKit = PhoneNumberKit()
    
    //MARK:- VIEW DID LOAD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startMonitoringInternet()
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])

    }
    
   //MARK:- PhoneNumberKit
    func isValidPhoneNumber(strPhone:String) -> Bool{
        do {
            _ = try phoneNumberKit.parse(strPhone)
            return true
        }
        catch {
            print("Generic parser error")
            return false
        }
    }

    
    
    //MARK:- BUTTON ACTION
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
      
        if AppUtility!.isEmpty(self.tfPhoneNumber.text!){
            AppUtility?.showToast(string: NSLocalizedString("validation_empty_PhoneNumber", comment: ""), view: self.view)
            self.tfPhoneNumber.shake()
            return
        }
        
        let trimmedString  = AppUtility?.validate(self.tfPhoneNumber.text!)
        let PhoneNumber = "\(self.lblCountryCode.text!)\(trimmedString!)"
        print(PhoneNumber)
        if !self.isValidPhoneNumber(strPhone: PhoneNumber){
            self.tfPhoneNumber.shake()
            self.btnContinue.isLoading =  false
            AppUtility?.showToast(string: NSLocalizedString("validation_PhoneNumber_Error", comment: ""), view: self.view)
            return
        }
        self.phoneNumberVerifyApi(strPhoneNumner: PhoneNumber)
    }
    
    @IBAction func termConditionButtonPressed(_ sender: UIButton) {
       
    }
    
    //MARK:- API Handler
    
    func phoneNumberVerifyApi(strPhoneNumner:String){
        
        if AppUtility!.connected() == false{
            return
        }
        self.btnContinue.isLoading =  true
        
        ApiHandler.sharedInstance.verifyPhoneNumber(verify:"0",phone:strPhoneNumner,strCode:"") { (isSuccess, resp) in
print(resp)
            if isSuccess {
                self.btnContinue.isLoading =  false
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    let vc =  self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
                    vc.strPhoneNumber =  strPhoneNumner
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
            }else{
                print(resp as Any)
            }
        }
    }
    
}
