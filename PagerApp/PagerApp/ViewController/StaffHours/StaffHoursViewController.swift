//
//  StaffHoursViewController.swift
//  PagerApp
//
//  Created by Mac on 17/11/2021.
//

import UIKit

class StaffHoursViewController: UIViewController {
    
    
    //MARK:- OUTLET
    
    @IBOutlet weak var calendarMonthView: UIView!
    @IBOutlet weak var lblCalendarMonth: UILabel!
    @IBOutlet weak var lblDateMonth: UILabel!
    @IBOutlet weak var btnCircle: UIButton!
    @IBOutlet weak var imgCircleTicked: UIImageView!
    @IBOutlet weak var btnCalendarMonth: UIButton!
    @IBOutlet weak var HoursPickerView: UIDatePicker!
    
    @IBOutlet weak var monthDaysView: UIView!
    @IBOutlet weak var lbl30Days: UILabel!
    @IBOutlet weak var lblDate30: UILabel!
    @IBOutlet weak var btnCircleMonth: UIButton!
    @IBOutlet weak var imgCircleMonthTicked: UIImageView!
    @IBOutlet weak var btnMonth30Day: UIButton!
    @IBOutlet weak var btnCreate: RNLoadingButton!
    
    
    var myUser:[User]?{didSet{}}
    var mySwitchBusiness:[switchBusiness]?{didSet{}}
    
    var month_calculation = "1"
    var workingHours = ""
    var objBusiness = [String:Any]()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    
    //MARK:- SETUP FUNCTION
    
    func setupView(){
        self.btnCreate.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        calendarMonthView.layer.borderWidth = 1.5
        calendarMonthView.layer.borderColor = UIColor.primary.cgColor
        lblCalendarMonth.textColor = UIColor.primary
        calendarMonthView.layer.cornerRadius = 5
        
        monthDaysView.layer.borderWidth = 1.5
        monthDaysView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        monthDaysView.layer.cornerRadius = 5
        self.myUser = User.readUserFromArchive()
        self.dobSetup()
        
    }
    
    //MARK:- Picker View
    
    private func dobSetup(){
        
        if #available(iOS 13.4, *) {
            HoursPickerView.preferredDatePickerStyle = .wheels
        }
        HoursPickerView.addTarget(self, action: #selector(dobDateChanged(_:)), for: .valueChanged)
        self.workingHours = (AppUtility?.timeString(time:HoursPickerView.countDownDuration))!
    }
    
    //MARK:- DATE PICKER ACTION
    
    @objc func dobDateChanged(_ sender: UIDatePicker) {
        sender.datePickerMode = UIDatePicker.Mode.countDownTimer
        self.workingHours = (AppUtility?.timeString(time:sender.countDownDuration))!
        
    }
    //MARK:- BUTTON ACTION
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createButtonPressed(_ sender: UIButton) {
        self.addBusiness()
    }
    
    @IBAction func calendarMonthButtonPressed(_ sender: UIButton) {
        
        lblCalendarMonth.textColor = UIColor.primary
        calendarMonthView.layer.borderColor = UIColor.primary.cgColor
        imgCircleTicked.isHidden = false
        imgCircleTicked.image = UIImage(named:"circle_tick")
        
        lbl30Days.textColor = .black
        monthDaysView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        imgCircleMonthTicked.isHidden = true
        self.month_calculation = "1"
    }
    
    @IBAction func month30DayButtonPressed(_ sender: UIButton) {
        
        lbl30Days.textColor = UIColor.primary
        monthDaysView.layer.borderColor = UIColor.primary.cgColor
        imgCircleMonthTicked.isHidden = false
        imgCircleMonthTicked.image = UIImage(named: "circle_tick")
        
        lblCalendarMonth.textColor = .black
        calendarMonthView.layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        imgCircleTicked.isHidden = true
        self.month_calculation = "2"
       
    }
    
    @IBAction func hrsPickerViewAction(_ sender: Any) {
       
    }
    //MARK:- API Handler
    
    func addBusiness(){
        
        if AppUtility!.connected() == false{
            return
        }
        self.btnCreate.isLoading =  true
        
        ApiHandler.sharedInstance.AddBusiness(business_name:objBusiness["businessname"] as! String,total_staff:objBusiness["staffnumber"] as! String,user_id:self.myUser![0].id!,month_calculation:self.month_calculation,username:objBusiness["name"] as! String,working_hours:self.workingHours) { (isSuccess, resp) in
            print("ress",resp)
            if isSuccess {
                self.btnCreate.isLoading =  false
                
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    
                    if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                        
                        self.showUserDetailApi(userID:(self.myUser?[0].id)!)
                       /* let BusinessObj = res["Business"] as! [String:Any]
                     
                        self.myUser = User.readUserFromArchive()
                    
                        switchBusinessObject.shared.Objresponse(response:   res)
                        
                        self.myUser?[0].business_id        =  BusinessObj["id"] as? String ?? "0"
                        self.myUser?[0].business_type_id   =  BusinessObj["business_type_id"] as? String ?? "0"
                        self.myUser?[0].user_id            =  BusinessObj["user_id"] as? String ?? "0"
                        self.myUser?[0].business_name      =  BusinessObj["name"] as? String ?? "0"
                        self.myUser?[0].total_staff        =  BusinessObj["total_staff"] as? String ?? "0"
                        self.myUser?[0].month_calculation  =  BusinessObj["month_calculation"] as? String ?? "0"
                        self.myUser?[0].working_hours      =  BusinessObj["working_hours"] as? String ?? "0"
                        self.myUser?[0].business_public_holiday_template_id =  BusinessObj["business_public_holiday_template_id"] as? String ?? "0"
                        self.myUser?[0].updated            =  BusinessObj["updated"] as? String ?? "0"
                        self.myUser?[0].business_created   =  BusinessObj["created"] as? String ?? "0"
                        self.myUser?[0].business_staff_count =  BusinessObj["business_staff_count"] as? Int ?? 0
                       
                        if User.saveUserToArchive(user:  self.myUser!){
                            print("New business account with:\(self.myUser?[0].business_name) has been added")
                            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            self.view.window?.rootViewController = nav
                        }*/
                    }else{
                        AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    }
                }else{
                    print(resp as Any)
                }
            }
        }
    }
    
    
    func showUserDetailApi(userID:String){
        
        if AppUtility!.connected() == false{
            return
        }
        ApiHandler.sharedInstance.showUserDetail(user_id: userID) { (isSuccess, resp) in
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                        UserObject.shared.Objresponse(response: res)
                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        let nav = UINavigationController(rootViewController: vc)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                        
                    }else{
                        AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    }
                }else{
                    print(resp as Any)
                }
            }
        }
    }
}
