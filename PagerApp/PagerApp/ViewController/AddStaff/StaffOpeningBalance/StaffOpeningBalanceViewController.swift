//
//  StaffOpeningBalanceViewController.swift
//  PagerApp
//
//  Created by Mac on 29/11/2021.
//

import UIKit

class StaffOpeningBalanceViewController: UIViewController {
    
    
    //MARK:- Outlets
    @IBOutlet weak var btnContinue: RNLoadingButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tfAmount: UITextField!
    var myUser:[User]? {didSet{}}
    var obj  = [String:Any]()
    var opening_balance_type = ""   //1 - advance 2- pending
    
    //MARK:-viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        print(obj)
        opening_balance_type = "1"
        self.myUser = User.readUserFromArchive()
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK:- Button Action
    
    @IBAction func segValueChanged(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            opening_balance_type = "1"
        }else{
            opening_balance_type = "2"
        }
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueAction(_ sender: Any) {
        self.addStaffApi()
    }
    
    //MARK:- API Handler
    
func addStaffApi(){
    let strId =   self.myUser?[0].business_id!
  
    
    if AppUtility!.connected() == false{
        return
    }
    self.btnContinue.isLoading =  true
    
    ApiHandler.sharedInstance.AddStaff(phone: staffPhonenumber, name: staffName, salary: salary, salary_cycle_date: salary_cycle_date, cycle_type: cycle_type, day: strday, opening_balance: self.tfAmount.text!, opening_balance_type: opening_balance_type, business_id: strId!, type_of_salary_payment: type_of_salary_payment, business_shift_id: "1"){ (isSuccess, resp) in
      
        if isSuccess {
            self.btnContinue.isLoading =  false
            
            if resp?.value(forKey: "code") as! NSNumber == 200{
                if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                }
            }else{
                print(resp as Any)
            }
        }
    }
  }
    
    
}
