//
//  AddStaffViewController.swift
//  PagerApp
//
//  Created by Mac on 18/11/2021.
//

import UIKit
import Alamofire
import ContactsUI
import Contacts

class AddStaffViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPhonenumber: UITextField!
    
    lazy var arrContacts = [CNContact]()
    lazy var contactStore = CNContactStore()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnAddContact.layer.borderColor = UIColor.primary.cgColor
        self.btnAddContact.layer.borderWidth = 1
        self.btnAddContact.layer.cornerRadius = 8
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func btnBackACtion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddContact(_ sender: Any) {
        self.onClickPickContact()
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        staffName =  self.tfUsername.text!
        staffPhonenumber =  self.tfPhonenumber.text!
        let vc =  storyboard?.instantiateViewController(withIdentifier: "SalaryPaymentTypeViewController") as! SalaryPaymentTypeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



extension AddStaffViewController:CNContactPickerDelegate{
    
    //MARK:- contact picker
    func onClickPickContact(){

        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
        [CNContactGivenNameKey
         , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
    public func contactPicker(_ picker: CNContactPickerViewController,
                              didSelect contactProperty: CNContactProperty) {
        
    }
    
    public func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        // user name
        let userName:String = contact.givenName
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        
        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
     
        self.tfUsername.text =  userName
        self.tfPhonenumber.text = primaryPhoneNumberStr
      
        print(primaryPhoneNumberStr)
      
        
    }
    
    public func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
}

