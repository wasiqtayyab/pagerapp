//
//  SraffWeeklyTableViewCell.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import UIKit

class StaffWeeklyTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblWeekName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupView(arrData:String){
        self.bgView.layer.cornerRadius =  5
        self.bgView.layer.borderColor =  UIColor.primary.cgColor
        self.bgView.layer.borderWidth =  1.0
        self.bgView.layer.backgroundColor = UIColor.ultralightGreen.cgColor
        self.lblWeekName.text = arrData
    }

}
