//
//  WeekViewController.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import UIKit
protocol salaryWeekCycle:class{
    func selectStaffWeek(salaryCycleWeek:String)
}

class StaffWeekViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    @IBOutlet weak var tbl: UITableView!
    var arrWeek = [["Week":"Monday"],["Week":"Tuesday"],["Week":"Wednesday"],["Week":"Thrusday"],["Week":"Friday"],["Week":"Saturday"],["Week":"Sunday"]]
    var DelegateSelectStaffWeek : salaryWeekCycle!
    
    //MARK: Outlets
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        
    }
    
    //MARK: Switch Action
    
    //MARK: Button Action
    
    //MARK: DELEGATE METHODS
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrWeek.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "StaffWeeklyTableViewCell") as! StaffWeeklyTableViewCell
        cell.setupView(arrData: self.arrWeek[indexPath.row]["Week"] as! String)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DelegateSelectStaffWeek.selectStaffWeek(salaryCycleWeek: self.arrWeek[indexPath.row]["Week"] as! String)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    //MARK: CollectionView.
    
    //MARK: Segment Control
    
    //MARK: Alert View
    
    //MARK: TextField
    
    //MARK: Location
    
    //MARK: Google Maps
    
    
}
