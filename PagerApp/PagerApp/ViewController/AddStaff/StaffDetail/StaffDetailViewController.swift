//
//  StaffDetailViewController.swift
//  PagerApp
//
//  Created by Mac on 29/11/2021.
//

import UIKit




class StaffDetailViewController: UIViewController,salaryCycleDate,salaryWeekCycle {
 
    
    
    
    //MARK:- Outlets
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tfSalaryAmount: UITextField!
    @IBOutlet weak var tfSalarycycle: UITextField!
    var arrobj  = [[String:Any]]()
    
    //MARK:- ViewDidLoad
    
    override func viewDidLoad() {
          super.viewDidLoad()
          
        self.setupView()
      }
      
    override func viewWillAppear(_ animated: Bool) {
     
    }
    
      //MARK:- SetupView
      
      func setupView(){
          
          if type_of_salary_payment == "Weekly"{
              self.tfSalarycycle.text = "Monday"
              strday = "Monday"
              cycle_type = "weekly"
          }else{
              strday = ""
              cycle_type = "monthly"
              self.tfSalarycycle.text = "\(salary_cycle_date) to \(salary_cycle_date) of Every Month"
          }
          
          self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
      }
      
    
    //MARK:- Delegate
    
    func selectStaffDay(salaryCycleDate: String) {
        strday = ""
        cycle_type = "monthly"
        salary_cycle_date = salaryCycleDate
        self.tfSalarycycle.text = "\(salaryCycleDate) to \(salaryCycleDate) of Every Month"
    }
    func selectStaffWeek(salaryCycleWeek: String) {
        salary_cycle_date = ""
        cycle_type = "weekly"
        strday = salaryCycleWeek
        self.tfSalarycycle.text = salaryCycleWeek
       
       
    }
    
    
      //MARK:- Button Action

    @IBAction func btnSelectSalaryCycle(_ sender: Any){
        
        if type_of_salary_payment != "Weekly"{
            let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffDaysViewController") as! StaffDaysViewController
            vc.delegateSelectDay = self
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffWeekViewController") as! StaffWeekViewController
            vc.DelegateSelectStaffWeek = self
            self.present(vc, animated: true, completion: nil)
        }
        
       
        
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueAction(_ sender: Any) {
        salary =  self.tfSalaryAmount.text!
     
        let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffOpeningBalanceViewController") as! StaffOpeningBalanceViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

      //MARK: CollectionView.
      
      //MARK: Segment Control
      
      //MARK: Alert View
      
      //MARK: TextField
      
      //MARK: Location
        
      //MARK: Google Maps
    
     
}
