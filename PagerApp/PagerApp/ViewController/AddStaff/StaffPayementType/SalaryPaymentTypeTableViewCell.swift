//
//  StaffPaymentTypeTableViewCell.swift
//  PagerApp
//
//  Created by Mac on 29/11/2021.
//

import UIKit

class SalaryPaymentTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var gbView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    
    
    func setupView(arrdata:[String:Any]){
       self.lblTitle.text =  arrdata["title"] as! String
       self.lblDescription.text = arrdata["description"] as! String
        self.gbView.layer.cornerRadius =  5
        if arrdata["isSelected"] as! String == "1"{
            self.lblTitle.textColor = UIColor.primary
            self.imgStatus.image = UIImage(named: "circle_tick")
            self.gbView.layer.borderColor =  UIColor.primary.cgColor
            self.gbView.layer.borderWidth =  1.0
        }else{
            self.lblTitle.textColor = UIColor.darkGray
            self.imgStatus.image = UIImage(named: "Round icon-1")
            self.gbView.layer.borderColor =  UIColor.lightGray.cgColor
            self.gbView.layer.borderWidth =  1.0
        }
    }
}
