//
//  SalaryPaymentTypeViewController.swift
//  PagerApp
//
//  Created by Mac on 29/11/2021.
//

import UIKit

class SalaryPaymentTypeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {


    
    //MARK:- Outlets
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tbl: UITableView!
    
    var arrType = [["title":"Monthly","description":"Fixed Salary, Salary paid per month","isSelected":"1","type":"monthly"],["title":"Hourly","description":"Punch In / Punch out time, Hourly payment system","isSelected":"0","type":"hourly"],["title":"Daily","description":"Payment based on number of days worked","isSelected":"0","type":"daily"],["title":"Weekly","description":"Weekly based on number of days worked","isSelected":"0","type":"weekly"],["title":"Work basis","description":"Payment based on number of units worked on","isSelected":"0","type":"work_basis"]]
  
   
    
    override func viewDidLoad() {
          super.viewDidLoad()
          
          self.setupView()
      }
      
      //MARK:- SetupView
      
      func setupView(){
          self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
      }
      
      //MARK:- Button Action

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueAction(_ sender: Any) {
  
        let vc =  storyboard?.instantiateViewController(withIdentifier: "StaffDetailViewController") as! StaffDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

      //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalaryPaymentTypeTableViewCell") as! SalaryPaymentTypeTableViewCell
        cell.setupView(arrdata: self.arrType[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 122
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for var i  in 0..<self.arrType.count{
            var obj =  self.arrType[i]
            obj.updateValue("0", forKey: "isSelected")
            self.arrType.remove(at: i)
            self.arrType.insert(obj, at: i)
        }
        var obj =  self.arrType[indexPath.row]
        obj.updateValue("1", forKey: "isSelected")
        self.arrType.remove(at: indexPath.row)
        self.arrType.insert(obj, at: indexPath.row)
        type_of_salary_payment =   self.arrType[indexPath.row]["type"] as! String
        
        self.tbl.reloadData()
    }
      //MARK: CollectionView.
      
      //MARK: Segment Control
      
      //MARK: Alert View
      
      //MARK: TextField
      
      //MARK: Location
        
      //MARK: Google Maps
    
     
}
