//
//  StaffDaysViewController.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import UIKit
protocol salaryCycleDate:class{
    func selectStaffDay(salaryCycleDate:String)
}

class StaffDaysViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
   
    
    
    //MARK: Outlets
    @IBOutlet weak var collectionView:UICollectionView!
    var delegateSelectDay:salaryCycleDate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
       
    }
    
    
    //MARK: CollectionView.
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "StaffDaysCollectionViewCell", for: indexPath) as! StaffDaysCollectionViewCell
        cell.bgView.layer.cornerRadius =  5
        cell.bgView.layer.borderColor =  UIColor.primary.cgColor
        cell.bgView.layer.backgroundColor = UIColor.ultralightGreen.cgColor
        cell.bgView.layer.borderWidth =  1.0
        cell.lblCount.text =  "\(indexPath.row + 1)"
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size  =  collectionView.layer.bounds.width/4
        return CGSize(width:70, height: 70)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        delegateSelectDay.selectStaffDay(salaryCycleDate:  "\(indexPath.row + 1)")
        self.dismiss(animated: true, completion: nil)
    }
    
}
