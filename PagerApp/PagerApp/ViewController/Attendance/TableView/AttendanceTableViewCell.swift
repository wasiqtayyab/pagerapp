//
//  AttendanceTableViewCell.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 03/12/2021.
//

import UIKit

class AttendanceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnPresent: UIButton!
    @IBOutlet weak var btnAbsent: UIButton!
    @IBOutlet weak var btnHalfDay: UIButton!
    @IBOutlet weak var btnArrow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnPresent.layer.borderWidth = 1.5
        btnPresent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnPresent.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
       
        btnAbsent.layer.borderWidth = 1.5
        btnAbsent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnAbsent.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
        
        btnHalfDay.layer.borderWidth = 1.5
        btnHalfDay.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnHalfDay.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
    
    
    @IBAction func absentButtonPressed(_ sender: UIButton) {
        btnAbsent.setTitleColor(.white, for: .normal)
        btnHalfDay.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnPresent.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnAbsent.backgroundColor = #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1)
        btnAbsent.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        btnAbsent.layer.borderWidth = 0
        
        btnHalfDay.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnHalfDay.layer.borderWidth = 1.5
        btnHalfDay.backgroundColor = .clear
        
        btnPresent.layer.borderWidth = 1.5
        btnPresent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnPresent.backgroundColor = .clear
    }
    
    @IBAction func halfDayButtonPressed(_ sender: UIButton) {
        btnHalfDay.setTitleColor(.white, for: .normal)
        btnAbsent.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnPresent.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnHalfDay.backgroundColor = #colorLiteral(red: 1, green: 0.6235294118, blue: 0.03921568627, alpha: 1)
        btnHalfDay.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        btnHalfDay.layer.borderWidth = 0
        
        btnPresent.layer.borderWidth = 1.5
        btnPresent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnPresent.backgroundColor = .clear
        
        btnAbsent.layer.borderWidth = 1.5
        btnAbsent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnAbsent.backgroundColor = .clear
    }
    
    @IBAction func presentButtonPressed(_ sender: UIButton) {
        btnPresent.setTitleColor(.white, for: .normal)
        btnAbsent.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnHalfDay.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
        btnPresent.backgroundColor = #colorLiteral(red: 0.1882352941, green: 0.8196078431, blue: 0.3450980392, alpha: 1)
        btnPresent.titleLabel!.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
        btnPresent.layer.borderWidth = 0
        
        btnAbsent.layer.borderWidth = 1.5
        btnAbsent.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnAbsent.backgroundColor = .clear
        
        btnHalfDay.layer.borderWidth = 1.5
        btnHalfDay.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        btnHalfDay.backgroundColor = .clear
    }
    
    @IBAction func btnArrowAction(_ sender: Any) {
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "StaffAttendenceStatusViewController") as!  StaffAttendenceStatusViewController
            rootViewController.navigationController?.present(vc, animated: true)
        }
    
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
