//
//  StaffAttendenceStatusTableViewCell.swift
//  PagerApp
//
//  Created by Mac on 24/12/2021.
//

import UIKit

class StaffAttendenceStatusTableViewCell: UITableViewCell {

    
    @IBOutlet weak var CircleView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CircleView.layer.cornerRadius  = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
