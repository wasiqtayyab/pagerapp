//
//  StaffAttendenceStatusViewController.swift
//  PagerApp
//
//  Created by Mac on 24/12/2021.
//

import UIKit

class StaffAttendenceStatusViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    
    //MARK: Outlets
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tblNotificatios: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet var viewsCollection: [UIView]!
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var viewEffect: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var tfNote: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        viewsCollection.forEach { (view) in
            view.layer.borderWidth = 1.0
            view.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.6228094038)
            view.layer.cornerRadius =  10
        }
        self.btnDone.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.tblHeight.constant = 80
    }

    //MARK: touchesBegan
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == viewEffect {
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "StaffAttendenceStatusTableViewCell") as! StaffAttendenceStatusTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    //MARK: Button Actions
    
    @IBAction func btnLateFineAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LateFineViewController") as! LateFineViewController
        vc.isModalInPopover = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnOverTimeAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "OverTimeViewController") as! OverTimeViewController
        vc.isModalInPopover = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnPaidLeaveAction(_ sender: Any) {
   
    }
    
}
