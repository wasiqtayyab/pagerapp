//
//  LateFineViewController.swift
//  PagerApp
//
//  Created by Mac on 27/12/2021.
//

import UIKit

class LateFineViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnSave: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnSave.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK: Button Action
    @IBAction func btnSaveAction(_ sender: Any) {
        
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true,completion:nil)
    }
    @IBAction func dateCounter(_ sender: UIDatePicker) {
        sender.datePickerMode = UIDatePicker.Mode.countDownTimer
        print(sender.countDownDuration)
    }
}
