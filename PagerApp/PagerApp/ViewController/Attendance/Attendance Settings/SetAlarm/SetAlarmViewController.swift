//
//  SetAlarmViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 10/12/2021.
//

import UIKit

protocol selectAlarmTime:class{
    func selectAlarmTime(setAlarm:String)
    
}

class SetAlarmViewController: UIViewController {
    
    //MARK: OUTLET
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var viewBlurEffect: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var deleteHeightConstraint: NSLayoutConstraint!
    var delegate:selectAlarmTime!
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnDelete.layer.borderWidth = 1.5
        self.btnDelete.layer.borderColor = #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1)
        self.btnSave.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        
    }
    //MARK: touchesBegan
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
       if touch?.view == viewBlurEffect {
            dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: BUTTON ACTION

    @IBAction func btnSaveAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
    
        sender.datePickerMode = UIDatePicker.Mode.time
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "EN")
        dateFormatter.dateFormat = "hh:mm"
        let selectedTime = dateFormatter.string(from: sender.date)
        
        print("Select date&Time:\(selectedTime)")
        delegate.selectAlarmTime(setAlarm: selectedTime)
        
    }
}
