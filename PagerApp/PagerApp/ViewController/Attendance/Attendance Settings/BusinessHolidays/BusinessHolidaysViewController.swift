//
//  BusinessHolidaysViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 21/12/2021.
//

import UIKit
protocol selectWeek:class{
    func selectWeek(selectWeek:[String])
    
}
class BusinessHolidaysViewController: UIViewController{
    //MARK:- OUTLET
    
    @IBOutlet weak var tblBusinessHolidays: UITableView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var blurViewEffect: UIView!
    @IBOutlet weak var btnSave: UIButton!
    
    
    //MARK:- VARIABLE DECLARATION
    
    var delegateWeek: selectWeek!
    var weekArr = [["week":"Sunday","isSelected":"0","abbreviation":"Sun"],
                   ["week":"Monday","isSelected":"0","abbreviation":"Mon"],
                   ["week":"Tuesday","isSelected":"0","abbreviation":"Tue"],
                   ["week":"Wednesday","isSelected":"0","abbreviation":"Wed"],
                   ["week":"Thursday","isSelected":"0","abbreviation":"Thru"],
                   ["week":"Friday","isSelected":"0","abbreviation":"Fri"],
                   ["week":"Saturday","isSelected":"0","abbreviation":"Sat"]]
    var selectWeekArr = [String]()
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSave.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        tblBusinessHolidays.delegate = self
        tblBusinessHolidays.dataSource = self
        tblHeightConstraint.constant = CGFloat(weekArr.count * 50)
    }
    
    //MARK: touchesBegan
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == blurViewEffect {
            dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        delegateWeek.selectWeek(selectWeek:self.selectWeekArr)
        dismiss(animated: true, completion: nil)
    }
    


}

extension BusinessHolidaysViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessHolidaysTableViewCell", for: indexPath)as! BusinessHolidaysTableViewCell
        cell.lblWeeks.text = weekArr[indexPath.row]["week"] as! String
        if weekArr[indexPath.row]["isSelected"] == "0"{
            cell.imgTicked.isHidden = true
        }else {
            cell.imgTicked.isHidden = false
            
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        var obj = self.weekArr[indexPath.row]
        if  weekArr[indexPath.row]["isSelected"] == "0"{
            obj.updateValue("1", forKey: "isSelected")
        }else {
            obj.updateValue("0", forKey: "isSelected")
        }
        
        self.weekArr.remove(at: indexPath.row)
        self.weekArr.insert(obj, at: indexPath.row)
        self.selectWeekArr.removeAll()
        for obj in self.weekArr{
            if obj["isSelected"] as! String == "1"{
                self.selectWeekArr.append(obj["abbreviation"] as!String)
            }
        }
        self.tblBusinessHolidays.reloadData()
        print(self.selectWeekArr)
    }
    
}
