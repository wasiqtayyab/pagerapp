//
//  OverTimeViewController.swift
//  PagerApp
//
//  Created by Mac on 27/12/2021.
//

import UIKit

class OverTimeViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: Outlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnHalf: UIButton!
    @IBOutlet weak var btnFullDay: UIButton!
    @IBOutlet var btnWorkedFor: [UIButton]!
    @IBOutlet weak var tfEnterWage: UITextField!
    @IBOutlet weak var lblTotalAmount: UILabel!
    var selectedHrs = "00:01"
    var enterWages  = "0.00"
    
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.btnWorkedFor.forEach { (btnWork) in
            btnWork.layer.borderWidth = 1.0
            btnWork.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        self.tfEnterWage.delegate =  self

    }
    
    //MARK: SetupView
    
    func setupView(){
        self.btnSave.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
    }
    
    //MARK: Button Action
    @IBAction func btnWorkedForAction(_ sender: UIButton) {
        if sender.tag == 0 {
            self.btnHalf.layer.borderColor = UIColor.primary.cgColor
            self.btnHalf.layer.backgroundColor = UIColor.ultralightGreen.cgColor
            
            self.btnFullDay.layer.borderColor = UIColor.lightGray.cgColor
            self.btnFullDay.layer.backgroundColor = UIColor.clear.cgColor
        }else {
            self.btnFullDay.layer.borderColor = UIColor.primary.cgColor
            self.btnFullDay.layer.backgroundColor = UIColor.ultralightGreen.cgColor
            
            self.btnHalf.layer.borderColor = UIColor.lightGray.cgColor
            self.btnHalf.layer.backgroundColor = UIColor.clear.cgColor
        }
    }
    @IBAction func btnSaveAction(_ sender: Any) {
        
    }
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true,completion:nil)
    }
    @IBAction func dateCounter(_ sender: UIDatePicker) {
        sender.datePickerMode = UIDatePicker.Mode.countDownTimer
        print(sender.countDownDuration)

        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "EN")
        dateFormatter.dateFormat = "hh:mm"
        let selectedTime = dateFormatter.string(from: sender.date)
        print("Select date&Time:\(selectedTime)")
        self.lblTotalAmount.text = "\(  self.enterWages) X \(self.selectedHrs) = $0:00"
    }
    @IBAction func tfEnterWage(_ sender: Any) {
        self.enterWages =  self.tfEnterWage.text!
        self.lblTotalAmount.text = "\(self.tfEnterWage.text!) X \(self.selectedHrs) = $0:00"
    }
}
