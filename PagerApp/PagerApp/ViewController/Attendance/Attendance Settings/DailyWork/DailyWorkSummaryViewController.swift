//
//  DailyWorkSummaryViewController.swift
//  PagerApp
//
//  Created by Mac on 21/12/2021.
//

import UIKit



class DailyWorkSummaryViewController: UIViewController {
    
    
    //MARK: Outlets
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var tbl: UITableView!
    var myUser:[User]?{didSet{}}
    var arrShowStaff:[showStaffDetail] = []
    var objectArray = [Objects]()
  
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    //MARK: SetupView
    
    func setupView(){
        self.tbl.register(UINib(nibName: "HeaderViewDailyWork", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderViewDailyWork")
        self.btnConfirm.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.showAllStaffs()
    }
    
    //MARK: Switch Action
    
    //MARK: Button Action
    @IBAction func btnConfirmAction(_ sender: Any) {
        
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK: TableView
extension DailyWorkSummaryViewController: UITableViewDataSource,UITableViewDelegate{
    
    
    //MARK: TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objectArray.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objectArray[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "staffTableViewCell") as! staffTableViewCell
        cell.setupData(arrData: self.objectArray[indexPath.section].sectionObjects[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "StaffProfileDetailViewController") as! StaffProfileDetailViewController
        vc.hidesBottomBarWhenPushed =  true
        vc.navigationTitle = self.objectArray[indexPath.section].sectionObjects[indexPath.row].staffname!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewDailyWork") as? HeaderViewDailyWork
        header?.lblSection.text = "\(self.objectArray[section].sectionName.capitalizingFirstLetter())(\(self.objectArray[section].sectionObjects.count))"
        return header
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
        //MARK: API Handler
        
        func showAllStaffs(){
            self.myUser = User.readUserFromArchive()

            if AppUtility!.connected() == false{
                return
            }
            let currentDate = NSDate()
            let formater = DateFormatter()
            formater.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let strdate =  formater.string(from: currentDate as Date)
            
            let strId =  self.myUser?[0].business_id!
            ApiHandler.sharedInstance.showAllStaffs(business_id: strId!,strDate:strdate) { (isSuccess, resp) in
                print(resp)
                if isSuccess {
                    if resp?.value(forKey: "code") as! NSNumber == 200{
                        if let res =  resp?.value(forKey: "msg") as? NSArray{
                            
                            for var i in 0 ..< res.count {
                                let dict =  res.object(at:i) as! NSDictionary
                                let obj = showStaffDetails.shared.ObjUserDetailResponse(res: dict.allValues as! [String:Any])
                                self.arrShowStaff.append(obj)
                            }
                            
                            var section = self.arrShowStaff.map{$0.cycle_type as! String}
                            section.removeDuplicates()
                            self.objectArray = section.map{ category in
                                print("Cat: ", category)
                                let obj = self.arrShowStaff.filter{ ($0.cycle_type) == category}
                                return Objects(sectionName: category, sectionObjects: obj)
                                
                            }
                          
                            self.tbl.isHidden = false
                            self.tbl.reloadData()
                            
                        }else{
                            self.btnConfirm.isUserInteractionEnabled =  false
                            self.tbl.isHidden = true
                        }
                    }else{
                        AppUtility?.displayAlert(title:"Error" , messageText: "Something went wrong", delegate: self)
                    }
                }
            }
        }
}
