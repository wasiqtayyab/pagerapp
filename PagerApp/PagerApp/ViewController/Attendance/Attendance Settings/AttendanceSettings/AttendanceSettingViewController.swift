//
//  AttendanceSettingViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 09/12/2021.
//

import UIKit

class AttendanceSettingViewController: UIViewController, selectAlarmTime, selectWeek {
    
    //MARK: OUTLET
    
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var viewBlurEffect: UIView!
    @IBOutlet weak var attendanceView: UIView!
    @IBOutlet weak var lblWeeklyHolidays: UILabel!
    @IBOutlet weak var lblAlarmSet: UILabel!
    @IBOutlet weak var lblWeek: UILabel!
    
    
    //MARK: VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: BUTTON ACTION
    
    @IBAction func alarmButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SetAlarmViewController")as! SetAlarmViewController
        vc.delegate =  self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func weeklyHolidayButtonPressed(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BusinessHolidaysViewController")as! BusinessHolidaysViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegateWeek = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func dailySummaryButtonPressed(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DailyWorkSummaryViewController")as! DailyWorkSummaryViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    
    
    //MARK: touchesBegan
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == viewBlurEffect {
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    //MARK: function
    
    func selectAlarmTime(setAlarm: String) {
        print("alarmTime:\(setAlarm)")
        self.lblAlarmSet.text =  setAlarm
    }
    
    func selectWeek(selectWeek: [String]) {
        let obj =  selectWeek.joined(separator: ",")
        self.lblWeek.text = obj
    }
    
    
    
    
}
