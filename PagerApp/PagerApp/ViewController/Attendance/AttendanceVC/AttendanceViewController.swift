//
//  AttendanceViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 03/12/2021.
//

import UIKit

class AttendanceViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: OUTLET
    @IBOutlet weak var attendacneCollectionView: UICollectionView!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tblAttendance: UITableView!
    
    @IBOutlet weak var videoCollectionView: UICollectionView!
    
    @IBOutlet weak var btnRightDate: UIButton!
    @IBOutlet weak var btnLeftDate: UIButton!
    
    var count = 0
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        attendacneCollectionView.delegate = self
        attendacneCollectionView.dataSource = self
        videoCollectionView.delegate = self
        videoCollectionView.dataSource = self
        tblAttendance.delegate = self
        tblAttendance.dataSource = self
        tblHeightConstraint.constant = 1 * 100 + 60
        
        tblAttendance.tableFooterView = UIView()
        self.tblAttendance.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        calendarSetup()
    }
    
    //MARK: FUNCTION
    
    func calendarSetup(){
        
        let formatingDate = getFormattedDate(date: Date(), format: "d MMM, yyyy | EEE")
        lblDate.text = formatingDate
        
    }
    
    //MARK: Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == attendacneCollectionView{
            return 6
        }else {
            return 2
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == attendacneCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendanceCollectionViewCell", for: indexPath)as! AttendanceCollectionViewCell
            
            return cell
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendanceVideosCollectionViewCell", for: indexPath)as! AttendanceVideosCollectionViewCell
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == attendacneCollectionView{
            return CGSize(width: (attendacneCollectionView.frame.width - 40.0)/3.0, height: 60.0)
        }else {
            
            return CGSize(width: (videoCollectionView.frame.width)/1.5, height: 188)
            
        }
        
    }
    
    //MARK: TableView Delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceTableViewCell") as! AttendanceTableViewCell
        cell.tag = indexPath.row
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as? HeaderView
        let backgroundView = UIView(frame: header!.bounds)
        backgroundView.backgroundColor = UIColor.clear
        header!.backgroundView = backgroundView
        header?.lblSection.text = "DAILY STAFF (1)"
        return header
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    //MARK: BUTTON ACTION
    
    
    @IBAction func settingButtonPressed(_ sender: UIButton) {
        
        if let rootViewController = UIApplication.topViewController() {
            let storyMain = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyMain.instantiateViewController(withIdentifier: "AttendanceSettingViewController") as! AttendanceSettingViewController
            rootViewController.navigationController?.present(vc, animated: true, completion: nil)
            
        }
    }
    
    
    
    @IBAction func leftDateButtonPressed(_ sender: UIButton) {
        self.count =  count - 1
        if let date =  Date().getDateFor(days: count) {
            let formatingDate = getFormattedDate(date: date, format: "d MMM, yyyy | EEE")
            btnRightDate.setImage(UIImage(named: "setting123456"), for: .normal)
            self.btnRightDate.isUserInteractionEnabled =  true
            lblDate.text = formatingDate
        }
    }
    
    @IBAction func rightDateButtonPressed(_ sender: UIButton) {
        
        if count == 0{
            btnRightDate.setImage(UIImage(named: "setting-3"), for: .normal)
            self.btnRightDate.isUserInteractionEnabled =  false
            return
        }
        self.count =  count + 1
        if let date =  Date().getDateFor(days: count) {
            let formatingDate = getFormattedDate(date: date, format: "d MMM, yyyy | EEE")
            
            lblDate.text = formatingDate
            
            if count == 0{
                btnRightDate.setImage(UIImage(named: "setting-3"), for: .normal)
                self.btnRightDate.isUserInteractionEnabled =  false
                return
            }
            
        }
    }
    
    
    //MARK: FORMAT FUNCTION
    
    func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
    
    
}

extension Date {
    func getDateFor(days:Int) -> Date? {
        
        return Calendar.current.date(byAdding: .day, value: days, to: Date())
    }
    
}
