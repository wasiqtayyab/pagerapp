//
//  AddNewBusinessViewController.swift
//  PagerApp
//
//  Created by Wasiq Tayyab on 18/11/2021.
//

import UIKit

class AddNewBusinessViewController: UIViewController {
    
    //MARK:- OUTLET
    
    @IBOutlet weak var tfBusinessName: UITextField!
    @IBOutlet weak var tfNumberStaffs: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnTicked: UIButton!
    @IBOutlet weak var imgTicked: UIImageView!
    
    
    //MARK:- VARS
    var myUser:[User]?{didSet{}}
    var isTicked = true
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK:- SETUP VIEW
    
    func setupView(){
        self.myUser =  User.readUserFromArchive()
        imgTicked.isHidden = false
        
        
    }
    
    //MARK:- BUTTON ACTION
    
    @IBAction func tickedButtonPressed(_ sender: UIButton) {
        
        if isTicked == true {
            
            btnTicked.layer.borderColor = #colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1)
            btnTicked.layer.borderWidth = 1.5
            btnTicked.layer.cornerRadius = 8
            imgTicked.isHidden = true
            isTicked = false
        }else {
            
            imgTicked.isHidden = false
            isTicked = true
        }
    }
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        
        
         if AppUtility!.isEmpty(self.tfBusinessName.text!){
             AppUtility?.showToast(string: NSLocalizedString("validation_empty_business_name", comment: ""), view: self.view)
            self.tfBusinessName.shake()
             return
         }
         
        
         var obj = [String:Any]()
         obj = [
            "name": self.myUser?[0].name,
             "businessname" : self.tfBusinessName.text!,
             "staffnumber": self.tfNumberStaffs.text!
         ]
         let vc =  self.storyboard?.instantiateViewController(withIdentifier: "StaffHoursViewController") as! StaffHoursViewController
         vc.objBusiness = obj
         self.navigationController?.pushViewController(vc, animated: true)
         
    }
    
    @IBAction func termConditionButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
