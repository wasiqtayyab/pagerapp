//
//  SelectLanguageCollectionViewCell.swift
//  PagerApp
//
//  Created by Mac on 25/11/2021.
//

import UIKit

class SelectLanguageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblLanguage: UILabel!
    
    
    override class func awakeFromNib() {
     
    }
    
    func setupView(data:[String:Any]){
        self.bgView.layer.borderWidth = 2.0
        self.bgView.layer.cornerRadius =  10
        self.lblLanguage.text =  data["lan"] as! String
        
        if data["isSelected"] as! String == "1"{
            self.bgView.backgroundColor   =  UIColor.ultralightGreen
            self.bgView.layer.borderColor =  UIColor.primary.cgColor
            self.imgStatus.image = UIImage(named: "circle_tick")
            self.lblLanguage.textColor = UIColor.DarkGreen
        }else{
            self.bgView.layer.borderColor =  UIColor.lightGray.cgColor
            self.bgView.backgroundColor =  UIColor(named: "bgViewDarKColor")
            self.imgStatus.image = UIImage(named: "filled_circle")
            self.lblLanguage.textColor = UIColor.lightGray
           
        }
    }
}
