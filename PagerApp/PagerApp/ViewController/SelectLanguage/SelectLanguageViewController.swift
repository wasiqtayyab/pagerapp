//
//  SelectLanguageViewController.swift
//  PagerApp
//
//  Created by Mac on 25/11/2021.
//

import UIKit
import EFInternetIndicator


class SelectLanguageViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,InternetStatusIndicable{
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var collectionViewLang: UICollectionView!
    @IBOutlet weak var btnContinue: UIButton!
    
    //MARK:_ Variables
    var internetConnectionIndicator: InternetViewIndicator?
    var myUser:[User]?{didSet{}}
    var arrLanguage = [["lan":"English","isSelected":"1"],["lan":"Spanish","isSelected":"0"]]
    var lang = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startMonitoringInternet()
        self.setupView()
    }
    
    //MARK:- SetupView
    
    func setupView(){
        self.myUser = User.readUserFromArchive()
        self.btnContinue.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        
        if self.myUser != nil && self.myUser?.count != 0{
            self.showUserDetailApi(userID: (self.myUser?[0].id)!)
        }
        
    }
    
    //MARK:- Button actions
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        
        if self.lang == "Spanish"{
            self.btnContinue.shake()
            AppUtility?.showToast(string: "Spanish language not implemeted,\nPlease select english language", view: self.view)
            return
        }
        if self.myUser != nil && self.myUser?.count != 0{
            if  let role  = self.myUser?[0].Role{
                if role["name"] as! String ==  "admin"{
                    if self.myUser![0].Business?.count == 0{
                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "AddBusinessViewController") as! AddBusinessViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        let nav = UINavigationController(rootViewController: vc)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                    }
                }
            }
            
        }else{
            let vc =  storyboard?.instantiateViewController(withIdentifier: "LoginAccountViewController") as! LoginAccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    //MARK: CollectionView.
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLanguage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "SelectLanguageCollectionViewCell", for: indexPath) as! SelectLanguageCollectionViewCell
        cell.setupView(data: self.arrLanguage[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = collectionViewLang.frame.width / 2
        return CGSize(width: size, height:130)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.reloadData(index: indexPath.row)
        
    }
    
    //MARK: Delegate function
    
    func reloadData(index:Int){
        for var i  in 0 ..< self.arrLanguage.count{
            var obj  =  self.arrLanguage[i]
            obj.updateValue("0", forKey: "isSelected")
            self.arrLanguage.remove(at: i)
            self.arrLanguage.insert(obj, at: i)
        }
        
        var obj  =  self.arrLanguage[index]
        self.lang =  obj["lan"] as! String
        obj.updateValue("1", forKey: "isSelected")
        self.arrLanguage.remove(at: index)
        self.arrLanguage.insert(obj, at: index)
        self.collectionViewLang.reloadData()
    }
    
    //MARK: API Handler
    
    func showUserDetailApi(userID:String){
        
        if AppUtility!.connected() == false{
            return
        }
        ApiHandler.sharedInstance.showUserDetail(user_id: userID) { (isSuccess, resp) in
            if isSuccess {
                if resp?.value(forKey: "code") as! NSNumber == 200{
                    if let res =  resp?.value(forKey: "msg") as? [String:Any]{
                        UserObject.shared.Objresponse(response: res)
                    }else{
                        AppUtility?.displayAlert(title: "Alert", messageText: resp?.value(forKey: "msg")as!String, delegate: self)
                    }
                }else{
                    print(resp as Any)
                }
            }
        }
    }
}
