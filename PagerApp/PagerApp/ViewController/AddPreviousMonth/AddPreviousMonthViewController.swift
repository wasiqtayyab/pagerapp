//
//  AddPreviousMonthViewController.swift
//  PagerApp
//
//  Created by Mac on 01/12/2021.
//

import UIKit

class AddPreviousMonthViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnConfirm.layer.borderColor = UIColor.primary.cgColor
        self.btnConfirm.applyBtnGradient(colors: [UIColor.lightGreen.cgColor,UIColor.primary.cgColor,UIColor.DarkGreen.cgColor])
        self.btnCancel.layer.borderWidth = 1.0
        self.btnCancel.layer.borderColor = UIColor.primary.cgColor
    }
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    }
    
    //MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.changeBackgroundAni()
    }
    
    func changeBackgroundAni() {
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            
        }, completion:nil)
    }
    
    //MARK:- Button Actions
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnConfirmAction(_ sender: Any) {
    }
    

}
