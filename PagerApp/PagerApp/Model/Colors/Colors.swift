//
//  Colors.swift
//  PagerApp
//
//  Created by Mac on 18/11/2021.
//

import UIKit

enum Color : Int {
    
    case primary = 1
    
    case lightGreen = 2
    
    case DarkGreen = 3
    
    case lightGray = 4
    
    case ultralightGreen  = 5
   
    static func valueFor(id : Int) -> UIColor? {
        
        switch id {
            
        case self.primary.rawValue:
            return .primary
        
        case self.lightGreen.rawValue:
            return .lightGreen
            
        case self.DarkGreen.rawValue:
            return .DarkGreen
            
        case self.lightGray.rawValue:
            return .lightGray
            
        case self.ultralightGreen.rawValue:
            return .ultralightGreen
            
            
        default:
            return nil
        }
    }
}

extension UIColor {
    
    // Primary Color
    static var primary : UIColor {   //Theme Colour of app
        return #colorLiteral(red: 0.1607843137, green: 0.6196078431, blue: 0.1137254902, alpha: 1) //UIColor(red: 149/255, green: 116/255, blue: 205/255, alpha: 1)
    }
    static var lightGreen : UIColor {
        return #colorLiteral(red: 0.1607843137, green: 0.8274509804, blue: 0.1137254902, alpha: 1) //UIColor(red: 149/255, green: 117/255, blue: 205/255, alpha: 1)
    }
    static var ultralightGreen : UIColor {
        return #colorLiteral(red: 0.4509803922, green: 0.9803921569, blue: 0.4745098039, alpha: 0.1577515255) //UIColor(red: 149/255, green: 117/255, blue: 205/255, alpha: 1)
    }
    // Secondary Color
    static var DarkGreen : UIColor {
        return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1) // UIColor(red: 238/255, green: 98/255, blue: 145/255, alpha: 1)
    }
    
    static var lightGray : UIColor {
        return #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1) // UIColor(red: 238/255, green: 98/255, blue: 145/255, alpha: 1)
    }
    
  
    // Secondary Color
    static var lightBlue : UIColor {
        return UIColor(red: 38/255, green: 118/255, blue: 188/255, alpha: 1)
    }
    
    //Gradient Start Color
    
    static var startGradient : UIColor {
        return UIColor(red: 83/255, green: 173/255, blue: 46/255, alpha: 1)
    }
    
    //Gradient End Color
    
    static var endGradient : UIColor {
        return UIColor(red: 158/255, green: 178/255, blue: 45/255, alpha: 1)
    }
    
    // Blue Color
    
    static var brightBlue : UIColor {
        return UIColor(red: 40/255, green: 25/255, blue: 255/255, alpha: 1)
    }
    
    
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static let backgroundColor = UIColor.rgb(r: 21, g: 22, b: 33)
    static let outlineStrokeColor = UIColor.rgb(r: 234, g: 46, b: 111)
    static let trackStrokeColor = UIColor.rgb(r: 56, g: 25, b: 49)
    static let pulsatingFillColor = UIColor.rgb(r: 86, g: 30, b: 63)
    
}
