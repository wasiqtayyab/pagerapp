//
//  staffClearDuesTableFooterView.swift
//  PagerApp
//
//  Created by Mac on 01/12/2021.
//

import UIKit

class staffClearDuesTableFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var btnClearDues: UIButton!
    @IBOutlet weak var lblPendindDues: UILabel!
}
