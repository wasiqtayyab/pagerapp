//
//  HeaderView.swift
//  Pagar
//
//  Created by Mac on 29/11/2021.
//  Copyright © 2021 DinoSoftLabs. All rights reserved.
//

import UIKit

class HeaderViewDailyWork: UITableViewHeaderFooterView {

    @IBOutlet weak var lblSection: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
    
}
