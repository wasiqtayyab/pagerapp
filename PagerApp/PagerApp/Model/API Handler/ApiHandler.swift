//
//  ApiHandler.swift
//  Infotex
//
//  Created by Mac on 03/09/2021.
//  Copyright © 2021 Mac. All rights reserved.

import Foundation
import UIKit
import Alamofire

var BASE_URL = "https://tankhawa.apps.qboxus.com/tankhawa/"
let API_KEY = "156c4675-9608-4591-b2ec-427503464aac-123"
let API_BASE_URL = BASE_URL+"api/"

let headers: HTTPHeaders = [
    "Api-Key":API_KEY
    
]

private let SharedInstance = ApiHandler()

enum Endpoint : String {
    
    
    case showUserDetail             =  "showUserDetail"
    case verifyPhoneNo              =  "verifyPhoneNo"
    case addBusiness                =  "addBusiness"
    case addStaff                   =  "addStaff"
    case showStaff                  =  "showStaffs"   //Withpagination
    case showAllStaff               =  "showAllStaff"   //WithoutPagination
  
    
}
class ApiHandler:NSObject{
    
    let fcm = UserDefaults.standard.string(forKey: "DeviceToken") as? String ?? ""
    let ip = UserDefaults.standard.string(forKey: "ipAddress") as? String ?? ""
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    let deviceType = "iOS"
    
    class var sharedInstance : ApiHandler {
        return SharedInstance
    }
    
    override init() {
       
    }
    
    
    //MARK: showUserDetail
    
    func showUserDetail(user_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
            parameters = [
                
                "user_id"  : user_id,
            ]
       
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.showUserDetail.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK: verifyPhoneNumber
    
    func verifyPhoneNumber(verify:String,phone:String,strCode:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
        if verify == "0"{
            parameters = [
                
                "phone"  : phone,
                "verify" : verify,
            ]
        }else{
            parameters = [
                
                "phone"  : phone,
                "verify" : verify,
                "code"   : strCode,
                
                
            ]
        }
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.verifyPhoneNo.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    
    //MARK: AddBusiness
    
    func AddBusiness(business_name:String,total_staff:String,user_id:String,month_calculation:String,username:String,working_hours:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
      
            parameters = [
                
                "business_name"       : business_name,
                "total_staff"         : total_staff,
                "user_id"             : user_id,
                "month_calculation"   : month_calculation,
                "username"            : username,
                "working_hours"       : working_hours
                
            ]
        
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.addBusiness.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
  
    
    //MARK: AddStaff
    
    func AddStaff(phone:String,name:String,salary:String,salary_cycle_date:String,cycle_type:String,day:String,opening_balance:String,opening_balance_type:String,business_id:String,type_of_salary_payment:String,business_shift_id:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
      
            parameters = [
                
                "phone"                     : phone,
                "name"                      : name,
                "salary"                    : salary,
                "salary_cycle_date"         : salary_cycle_date,
                "cycle_type"                : cycle_type,
                "day"                       : day,
                "opening_balance"           : opening_balance,
                "opening_balance_type"      : opening_balance_type,
                "business_id"               : business_id,
                "type_of_salary_payment"    : type_of_salary_payment,
                "business_shift_id"         : business_shift_id,
                
            ]
        
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.addStaff.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    
    //MARK: Show Staffs
    
    func showStaffs(business_id:String,starting_point:String,strDate:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
            parameters = [
                
                "business_id"     : business_id,
                "starting_point"  : starting_point,
                "date"            : strDate
            ]
       
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.showStaff.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response.result)
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    //MARK: Show All Staffs
    
    func showAllStaffs(business_id:String,strDate:String,completionHandler:@escaping( _ result:Bool, _ responseObject:NSDictionary?)->Void){
        let headers: HTTPHeaders = [
            "Api-Key":API_KEY
        ]
        var parameters = [String : Any]()
        
            parameters = [
                
                "business_id" : business_id,
                "date"        : strDate
            ]
       
       
        let finalUrl = "\(API_BASE_URL)\(Endpoint.showAllStaff.rawValue)"
        
        print(finalUrl)
        print(parameters)
        AF.request(URL.init(string: finalUrl)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            switch response.result {
            
            case .success(_):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            case .failure(let error):
                if let json = response.value
                {
                    do {
                        let dict = json as? NSDictionary
                        completionHandler(true, dict)
                        AppUtility?.showToast(string: error.localizedDescription, view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
                        
                    } catch {
                        completionHandler(false, nil)
                    }
                }
                break
            }
        }
    }
    func stopAllSessions() {
        AF.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}
