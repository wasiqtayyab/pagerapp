//
//  switchBusinessObject.swift
//  PagerApp
//
//  Created by Mac on 23/12/2021.
//

import Foundation
import UIKit

class switchBusinessObject{
    
    var mySwitchBusiness: [switchBusiness]? {didSet {}}
    
    static let shared = switchBusinessObject() //  singleton object
    
    
    func Objresponse(response:[String:Any]){
        
        let SwitchBusiness = switchBusiness()
        
        if let BusinessObj = response["Business"] as? [[String:Any]]{
            
            
            for obj in BusinessObj{
                
                SwitchBusiness.id =  obj["id"] as? String ?? "0"
                SwitchBusiness.business_type_id =  obj["business_type_id"] as? String ?? "0"
                SwitchBusiness.user_id =  obj["user_id"] as? String ?? "0"
                SwitchBusiness.name =  obj["name"] as? String ?? "0"
                SwitchBusiness.total_staff =  obj["total_staff"] as? String ?? "0"
                SwitchBusiness.month_calculation =  obj["month_calculation"] as? String ?? "0"
                SwitchBusiness.working_hours =  obj["working_hours"] as? String ?? "0"
                SwitchBusiness.business_public_holiday_template_id =  obj["business_public_holiday_template_id"] as? String ?? "0"
                SwitchBusiness.updated =  obj["updated"] as? String ?? "0"
                SwitchBusiness.created =  obj["created"] as? String ?? "0"
                SwitchBusiness.business_staff_count =  obj["business_staff_count"] as? Int ?? 0
                self.mySwitchBusiness = switchBusiness.readswitchBusinessFromArchive()
                
                if mySwitchBusiness?.count == 0 || self.mySwitchBusiness == nil{
                    self.mySwitchBusiness = [SwitchBusiness]
                    if switchBusiness.saveswitchBusinessToArchive(switchBusiness: self.mySwitchBusiness!) {
                        print("Switch Account Saved One Object in Directory")
                    }
                }else{
                    self.mySwitchBusiness?.append(SwitchBusiness)
                    if switchBusiness.saveswitchBusinessToArchive(switchBusiness: self.mySwitchBusiness!) {
                        print("Switch Account Saved multiple Object in Directory")
                    }
                }
            }
        }else{
            
            let BusinessObj = response["Business"] as? [String:Any] ?? ["":""]
            
            SwitchBusiness.id =  BusinessObj["id"] as? String ?? "0"
            SwitchBusiness.business_type_id =  BusinessObj["business_type_id"] as? String ?? "0"
            SwitchBusiness.user_id =  BusinessObj["user_id"] as? String ?? "0"
            SwitchBusiness.name =  BusinessObj["name"] as? String ?? "0"
            SwitchBusiness.total_staff =  BusinessObj["total_staff"] as? String ?? "0"
            SwitchBusiness.month_calculation =  BusinessObj["month_calculation"] as? String ?? "0"
            SwitchBusiness.working_hours =  BusinessObj["working_hours"] as? String ?? "0"
            SwitchBusiness.business_public_holiday_template_id =  BusinessObj["business_public_holiday_template_id"] as? String ?? "0"
            SwitchBusiness.updated =  BusinessObj["updated"] as? String ?? "0"
            SwitchBusiness.created =  BusinessObj["created"] as? String ?? "0"
            SwitchBusiness.business_staff_count =  BusinessObj["business_staff_count"] as? Int ?? 0
            
            self.mySwitchBusiness = switchBusiness.readswitchBusinessFromArchive()
            
            if mySwitchBusiness?.count == 0 || self.mySwitchBusiness == nil{
                self.mySwitchBusiness = [SwitchBusiness]
                if switchBusiness.saveswitchBusinessToArchive(switchBusiness: self.mySwitchBusiness!) {
                    print("Switch Account Saved One Object in Directory")
                }
            }else{
                self.mySwitchBusiness?.append(SwitchBusiness)
                if switchBusiness.saveswitchBusinessToArchive(switchBusiness: self.mySwitchBusiness!) {
                    print("Switch Account Saved multiple Object in Directory")
                }
            }
        }
    }
}

//MARK:- Switch Business

class switchBusiness: NSObject, NSCoding {
    
    var id: String?
    var business_type_id: String?
    var user_id: String?
    var name: String?
    var total_staff: String?
    var month_calculation: String?
    var working_hours: String?
    var business_public_holiday_template_id: String?
    var updated: String?
    var created: String?
    var business_staff_count: Int?
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.business_type_id = aDecoder.decodeObject(forKey: "business_type_id") as? String
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.total_staff = aDecoder.decodeObject(forKey: "total_staff") as? String
        self.month_calculation = aDecoder.decodeObject(forKey: "month_calculation") as? String
        self.working_hours = aDecoder.decodeObject(forKey: "working_hours") as? String
        self.business_public_holiday_template_id = aDecoder.decodeObject(forKey: "business_public_holiday_template_id") as? String
        self.updated = aDecoder.decodeObject(forKey: "updated") as? String
        self.created = aDecoder.decodeObject(forKey: "created") as? String
        self.business_staff_count = aDecoder.decodeObject(forKey: "business_staff_count") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.business_type_id, forKey: "business_type_id")
        aCoder.encode(self.user_id, forKey: "user_id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.total_staff, forKey: "total_staff")
        aCoder.encode(self.month_calculation, forKey: "month_calculation")
        aCoder.encode(self.working_hours, forKey: "working_hours")
        aCoder.encode(self.business_public_holiday_template_id, forKey: "business_public_holiday_template_id")
        aCoder.encode(self.updated, forKey: "updated")
        aCoder.encode(self.created, forKey: "created")
        aCoder.encode(self.business_staff_count, forKey: "business_staff_count")
        
    }
    //MARK: Archive Methods
    class func archiveFilePath() -> String {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsDirectory.appendingPathComponent("switchBusiness.archive").path
    }
    
    class func readswitchBusinessFromArchive() -> [switchBusiness]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: archiveFilePath()) as? [switchBusiness]
    }
    
    class func saveswitchBusinessToArchive(switchBusiness: [switchBusiness]) -> Bool {
        return NSKeyedArchiver.archiveRootObject(switchBusiness, toFile: archiveFilePath())
    }
}
