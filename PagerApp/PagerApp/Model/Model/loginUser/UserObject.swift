//
//  UserObject.swift
//
//  Infotex
//
//  Created by Mac on 01/09/2021.
//  Copyright © 2021 Mac. All rights reserved.


import Foundation
import UIKit

class UserObject{
    
    var myUser: [User]? {didSet {}}
    var mySwitchBusiness: [switchBusiness]? {didSet {}}
    
    static let shared = UserObject() //  singleton object
    
    
    func Objresponse(response:[String:Any]){
        let RoleObj = response["Role"] as? [String:Any] ?? ["":""]
        let BusinessObj = response["Business"] as? [[String:Any]] ?? [["":""]]
        let UserObj = response["User"] as! NSDictionary
        
        let user = User()
        
        
        user.id =  UserObj.value(forKey: "id") as! String
        user.auth_token =  UserObj.value(forKey: "auth_token") as? String ?? ""
        user.created =  UserObj.value(forKey: "created") as? String ?? ""
        user.device_token =  UserObj.value(forKey: "device_token") as? String ?? ""
        user.name =  UserObj.value(forKey: "name") as? String ?? ""
        user.phone =  UserObj.value(forKey: "phone") as? String ?? ""
        user.role_id =  UserObj.value(forKey: "role_id") as? String ?? ""
        user.sms_report =  UserObj.value(forKey: "sms_report") as? String ?? "0"
        user.whatsapp_notification =  UserObj.value(forKey: "whatsapp_notification") as? String ?? "0"
        user.Role =  RoleObj
        user.Business = BusinessObj
        if BusinessObj.count > 0{
            user.business_id =  BusinessObj[0]["id"] as? String ?? "0"
            user.business_type_id =  BusinessObj[0]["business_type_id"] as? String ?? "0"
            user.user_id =  BusinessObj[0]["user_id"] as? String ?? "0"
            user.business_name =  BusinessObj[0]["name"] as? String ?? "0"
            user.total_staff =  BusinessObj[0]["total_staff"] as? String ?? "0"
            user.month_calculation =  BusinessObj[0]["month_calculation"] as? String ?? "0"
            user.working_hours =  BusinessObj[0]["working_hours"] as? String ?? "0"
            user.business_public_holiday_template_id =  BusinessObj[0]["business_public_holiday_template_id"] as? String ?? "0"
            user.updated =  BusinessObj[0]["updated"] as? String ?? "0"
            user.business_created =  BusinessObj[0]["created"] as? String ?? "0"
            user.business_staff_count =  BusinessObj[0]["business_staff_count"] as? Int ?? 0
        }
       
        
        self.myUser = [user]
        if User.saveUserToArchive(user: self.myUser!) {
            print("User Saved in Directory")
            self.myUser = User.readUserFromArchive()
          //  switchBusinessObject.shared.Objresponse(response:   response)
        }
    }
}


class User: NSObject, NSCoding {
    
    var id:String?
    var auth_token :String?
    var created :String?
    var device_token :String?
    var name :String?
    var phone :String?
    var role_id :String?
    var sms_report :String?
    var whatsapp_notification : String?
    var Role : [String:Any]?
    var Business : [[String:Any]]?
    
    var business_id: String?
    var business_type_id: String?
    var user_id: String?
    var business_name: String?
    var total_staff: String?
    var month_calculation: String?
    var working_hours: String?
    var business_public_holiday_template_id: String?
    var updated: String?
    var business_created: String?
    var business_staff_count: Int?
    
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
     
        self.id = aDecoder.decodeObject(forKey: "id") as? String
        self.auth_token = aDecoder.decodeObject(forKey: "auth_token") as? String
        self.created = aDecoder.decodeObject(forKey: "created") as? String
        self.device_token = aDecoder.decodeObject(forKey: "device_token") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.role_id = aDecoder.decodeObject(forKey: "role_id") as? String
        self.sms_report = aDecoder.decodeObject(forKey: "sms_report") as? String
        self.whatsapp_notification = aDecoder.decodeObject(forKey: "whatsapp_notification") as? String
        self.Role = aDecoder.decodeObject(forKey: "Role") as? [String:Any]
        self.Business = aDecoder.decodeObject(forKey: "Business") as? [[String:Any]]
        
        self.business_id = aDecoder.decodeObject(forKey: "business_id") as? String
        self.business_type_id = aDecoder.decodeObject(forKey: "business_type_id") as? String
        self.user_id = aDecoder.decodeObject(forKey: "user_id") as? String
        self.business_name = aDecoder.decodeObject(forKey: "business_name") as? String
        self.total_staff = aDecoder.decodeObject(forKey: "total_staff") as? String
        self.month_calculation = aDecoder.decodeObject(forKey: "month_calculation") as? String
        self.working_hours = aDecoder.decodeObject(forKey: "working_hours") as? String
        self.business_public_holiday_template_id = aDecoder.decodeObject(forKey: "business_public_holiday_template_id") as? String
        self.updated = aDecoder.decodeObject(forKey: "updated") as? String
        self.business_created = aDecoder.decodeObject(forKey: "business_created") as? String
        self.business_staff_count = aDecoder.decodeObject(forKey: "business_staff_count") as? Int
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.auth_token, forKey: "auth_token")
        aCoder.encode(self.created, forKey: "created")
        aCoder.encode(self.device_token, forKey: "device_token")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.role_id, forKey: "role_id")
        aCoder.encode(self.sms_report, forKey: "sms_report")
        aCoder.encode(self.whatsapp_notification, forKey: "whatsapp_notification")
        aCoder.encode(self.Role, forKey: "Role")
        aCoder.encode(self.Business, forKey: "Business")
        
        aCoder.encode(self.business_id, forKey: "business_id")
        aCoder.encode(self.business_type_id, forKey: "business_type_id")
        aCoder.encode(self.user_id, forKey: "user_id")
        aCoder.encode(self.business_name, forKey: "business_name")
        aCoder.encode(self.total_staff, forKey: "total_staff")
        aCoder.encode(self.month_calculation, forKey: "month_calculation")
        aCoder.encode(self.working_hours, forKey: "working_hours")
        aCoder.encode(self.business_public_holiday_template_id, forKey: "business_public_holiday_template_id")
        aCoder.encode(self.updated, forKey: "updated")
        aCoder.encode(self.business_created, forKey: "business_created")
        aCoder.encode(self.business_staff_count, forKey: "business_staff_count")
    
    }
    //MARK: Archive Methods
    class func archiveFilePath() -> String {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsDirectory.appendingPathComponent("user.archive").path
    }
    
    class func readUserFromArchive() -> [User]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: archiveFilePath()) as? [User]
    }
    
    class func saveUserToArchive(user: [User]) -> Bool {
        return NSKeyedArchiver.archiveRootObject(user, toFile: archiveFilePath())
    }
}


