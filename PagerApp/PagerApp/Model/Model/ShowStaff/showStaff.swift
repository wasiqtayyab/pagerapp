//
//  showStaff.swift
//  PagerApp
//
//  Created by Mac on 30/11/2021.
//

import Foundation


struct showStaff{

    var staffId :String?
    var name:String?
    var phone:String?
    var role_id:String?
    var business_id :String?
    var type_of_staff:String?
    var business_shift_id:String?
    var salary:Int?
    var salary_cycle_date :String?
    var cycle_type:String?
    var day:String?
    var opening_balance:String?
    var opening_balance_type :String?
    var type_of_salary_payment:String?
    var updated:String?
    var created:String?
    var objAttendencer:attendence?
}

struct attendence{
    var attendenceId :Int?
    
}
