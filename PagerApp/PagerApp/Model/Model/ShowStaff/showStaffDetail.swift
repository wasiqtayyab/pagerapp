//
//  BookedInfluencerDetail.swift
//  Infotex
//
//  Created by Mac on 12/10/2021.
//

import Foundation
import UIKit

class showStaffDetails{
    
    static let shared = showStaffDetails()
    
    func ObjUserDetailResponse(res:[String:Any]) -> showStaffDetail{
        
        let arrStaff = res["Staff"] as? [String:Any] ?? ["":""]
        let arrBusiness = res["Business"] as? [String:Any] ?? ["":""]
        let objAttendence = arrStaff["Attendance"] as? [String:Any] ?? ["":""]
        let arrAttendence = objAttendence["Attendance"] as? [String:Any] ?? ["":""]
     
        
        let data =  showStaffDetail(BusinessId: arrBusiness["id"] as! String, business_type_id: arrBusiness["business_type_id"] as? String ?? "", user_id: arrBusiness["user_id"] as? String ?? "", name: arrBusiness["name"] as? String ?? "", total_staff: arrBusiness["total_staff"] as? String ?? "", month_calculation: arrBusiness["month_calculation"] as? String ?? "", working_hours: arrBusiness["working_hours"] as? String ?? "", business_public_holiday_template_id: arrBusiness["business_public_holiday_template_id"] as? String ?? "", updated: arrBusiness["created"] as? String ?? "", created: arrBusiness["created"] as? String ?? "", staffId: arrStaff["id"] as? String ?? "", staffname: arrStaff["name"] as? String ?? "", phone: arrStaff["phone"] as? String ?? "", role_id: arrStaff["role_id"] as? String ?? "", business_id: arrStaff["business_id"] as? String ?? "", type_of_staff: arrStaff["type_of_staff"] as? String ?? "", business_shift_id: arrStaff["business_shift_id"] as? String ?? "", salary: arrStaff["salary_cycle_date"] as? Int ?? 0, salary_cycle_date: arrStaff["salary_cycle_date"] as? String ?? "", cycle_type: arrStaff["cycle_type"] as? String ?? "", day: arrStaff["day"] as? String ?? "", opening_balance: arrStaff["opening_balance"] as? String ?? "", opening_balance_type: arrStaff["opening_balance_type"] as? String ?? "", type_of_salary_payment: arrStaff["type_of_salary_payment"] as? String ?? "", staffUpdated:  arrStaff["updated"] as? String ?? "", staffCreated:  arrStaff["created"] as? String ?? "", attendenceId :arrAttendence["id"] as? Int ?? 0)
     
          return data
    }
}


struct showStaffDetail{
    var BusinessId :String?
    var business_type_id:String?
    var user_id:String?
    var name:String?
    var total_staff :String?
    var month_calculation:String?
    var working_hours:String?
    var business_public_holiday_template_id:String?
    var updated :String?
    var created:String?
    
    
    //Staff Data
    var staffId :String?
    var staffname:String?
    var phone:String?
    var role_id:String?
    var business_id :String?
    var type_of_staff:String?
    var business_shift_id:String?
    var salary:Int?
    var salary_cycle_date :String?
    var cycle_type:String?
    var day:String?
    var opening_balance:String?
    var opening_balance_type :String?
    var type_of_salary_payment:String?
    var staffUpdated:String?
    var staffCreated:String?
    var attendenceId :Int?
}
